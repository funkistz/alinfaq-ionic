// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: 'AIzaSyD2IXmquFpkdCInPPpGFLRKAUKpENM-VuA',
    authDomain: 'al-infaq-apps.firebaseapp.com',
    databaseURL: 'https://al-infaq-apps.firebaseio.com',
    projectId: 'al-infaq-apps',
    storageBucket: 'al-infaq-apps.appspot.com',
    messagingSenderId: '60755095741',
    appId: '1:60755095741:web:a31fc5d4a7911ba712c7d4',
    measurementId: 'G-JQL10FZEZJ'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
