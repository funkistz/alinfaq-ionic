import { Component } from '@angular/core';
import { LoadingController, NavController, ToastController, Platform } from '@ionic/angular';
import { DeviceOrientation, DeviceOrientationCompassHeading } from '@ionic-native/device-orientation/ngx';
import { Geolocation, Geoposition } from '@ionic-native/geolocation/ngx';
import { Diagnostic } from '@ionic-native/diagnostic/ngx';
import { AlertController } from '@ionic/angular';

@Component({
    selector: 'app-qibla',
    templateUrl: 'qibla.page.html',
    styleUrls: ['qibla.page.scss']
})
export class QiblaPage {

    public data: DeviceOrientationCompassHeading = null;
    public currentLocation: any = null;
    // Initial Kaaba location that we've got from google maps
    // private kaabaLocation: { lat: number, lng: number } = { lat: 21.42276, lng: 39.8256687 };
    private kaabaLocation: { lat: number, lng: number } = { lat: 21.45, lng: -39.75 };
    // Initial Qibla Location
    public qiblaLocation = 0;
    locationOn = 'pending';
    orientationOn = 'pending';

    constructor(
        private deviceOrientation: DeviceOrientation,
        private geolocation: Geolocation,
        public platform: Platform,
        private diagnostic: Diagnostic,
        public alertController: AlertController,
        private navCtrl: NavController
    ) {


    }

    ionViewDidEnter() {
        this.diagnosticLocation();
    }

    diagnosticLocation() {
        this.locationOn = 'pending';
        // in 3 seconds do something
        this.diagnostic.isLocationAvailable().then(this.locationSuccess, this.locationFail);
    }

    locationSuccess = (data) => {
        this.checkLocationState();
    }
    locationFail = (error) => {
        console.log(error);
        this.locationOn = 'na';
        this.presentAlert('Error!', 'No GPS found on your device.');

        if (this.platform.is('desktop')) {
            this.locationOn = 'on';
            this.startQibla();
        }
    }

    checkLocationState() {

        console.log('checking location state...');
        this.diagnostic.isLocationEnabled().then((success) => {

            if (success) {
                this.locationOn = 'on';
                this.startQibla();
            } else {
                this.locationOn = 'off';
                this.presentAlert('Error!', 'Please enable your location.');
            }
        }, (error) => {
            console.log(error);
            this.locationOn = 'off';
            this.presentAlert('Error!', 'Please enable your location.');
        });
    }

    startQibla() {
        // Watch the device compass heading change
        this.deviceOrientation.watchHeading().subscribe((res: DeviceOrientationCompassHeading) => {
            this.data = res;
            // Change qiblaLocation when currentLocation is not empty
            if (!!this.currentLocation) {
                const currentQibla = res.magneticHeading - this.getQiblaPosition();
                this.qiblaLocation = currentQibla > 360 ? currentQibla % 360 : currentQibla;
            }
        }, error => {

            this.orientationOn = 'na';
            this.presentAlert('Error!', 'Your device does not support device orientation.');


        });
        // Watch current location
        this.geolocation.watchPosition().subscribe((res) => {

            console.log('geolocation', res);
            const loc: any = res;

            if (loc && loc.coords) {
                console.log('location', res);
                this.currentLocation = res;

                if (this.platform.is('desktop')) {
                    console.log('getQiblaPosition', this.getQiblaPosition());
                    const currentQibla = 0 - this.getQiblaPosition();
                    this.qiblaLocation = currentQibla > 360 ? currentQibla % 360 : currentQibla;
                }

            }
        });
    }

    getQiblaPosition() {

        if (!this.currentLocation) {
            return 0;
        }

        console.log('getQiblaPosition');

        // const eq1 = Math.sin(this.currentLocation.coords.longitude - this.kaabaLocation.lng);
        // const eq2 = Math.cos(this.currentLocation.coords.latitude) * Math.tan(this.kaabaLocation.lat);
        // const eq3 = Math.sin(this.currentLocation.coords.latitude) *
        //     Math.cos(this.currentLocation.coords.longitude - this.kaabaLocation.lng);

        // return Math.atan((eq1 / (eq2 - eq3)));

        // Convert; all; geopoint; degree; to; radian; before; jump; to; furmula;
        const currentLocationLat = this.degreeToRadian(this.currentLocation.coords.latitude);
        const currentLocationLng = this.degreeToRadian(this.currentLocation.coords.longitude);
        const kaabaLocationLat = this.degreeToRadian(this.kaabaLocation.lat);
        const kaabaLocationLng = this.degreeToRadian(this.kaabaLocation.lng);

        // Use Basic Spherical Trigonometric Formula
        return this.radianToDegree(
            Math.atan2(
                Math.sin(kaabaLocationLng - currentLocationLng),
                (Math.cos(currentLocationLat) * Math.tan(kaabaLocationLat) - Math.sin(currentLocationLat) * Math.cos(kaabaLocationLng - currentLocationLng))
            )
        );
    }

    /**
     * Convert from Radian to Degree
     * @param radian
     */
    radianToDegree(radian: number) {
        return radian * 180 / Math.PI;
    }

    /**
     * Convert from Degree to Radian
     * @param degree
     */
    degreeToRadian(degree: number) {
        return degree * Math.PI / 180;
    }

    async presentAlert(title, message) {
        const alert = await this.alertController.create({
            header: title,
            message
        });

        await alert.present();

        this.navCtrl.pop();
    }
}
