import { Component, OnInit, ViewChild } from '@angular/core';
import { LoadingController, NavController, AlertController } from '@ionic/angular';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from '../api.service';
import { Data } from '../data';
import { Settings } from '../data/settings';
import { Product } from '../data/product';
import { OneSignal } from '@ionic-native/onesignal/ngx';
import { NativeStorage } from '@ionic-native/native-storage/ngx';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { Platform } from '@ionic/angular';
import { Config } from '../config';
import { TranslateService } from '@ngx-translate/core';
import { SolatService } from './../services/solat.service';
import * as moment from 'moment';
import { Storage } from '@ionic/storage';
import { StorageService } from '../services/storage/storage.service';
import { FirebaseService } from '../services/firebase.service';

@Component({
    selector: 'app-home',
    templateUrl: 'home.page.html',
    styleUrls: ['home.page.scss']
})
export class HomePage {
    tempProducts: any = [];
    filter: any = {};
    hasMoreItems = true;
    cart: any;
    screenWidth: any = 300;
    slideOpts = { effect: 'flip', autoplay: true, parallax: true, loop: true, lazy: true };

    highlights: any = [];
    featureds: any = [];
    prayerTime: any = {};
    language: any;

    @ViewChild('horizontalCardList', { static: false }) horizontalCardList;
    @ViewChild('horizontalCardList2', { static: false }) horizontalCardList2;

    constructor(
        private config: Config,
        public api: ApiService,
        private splashScreen: SplashScreen,
        public platform: Platform,
        public translateService: TranslateService,
        public data: Data,
        public settings: Settings,
        public product: Product,
        public loadingController: LoadingController,
        public router: Router,
        public navCtrl: NavController,
        public route: ActivatedRoute,
        private oneSignal: OneSignal,
        private nativeStorage: NativeStorage,
        private solatService: SolatService,
        public alertController: AlertController,
        private storage: Storage,
        private storageService: StorageService,
        private firebaseService: FirebaseService,
    ) {
        this.filter.page = 1;
        this.filter.status = 'publish';
        this.screenWidth = this.platform.width();

        this.storageService.getItem('language').then(language => {
            this.language = language;
        });

        console.log('settings', this.settings);
    }

    ionViewDidEnter() {

        this.horizontalCardList.ionViewDidEnter();
        this.horizontalCardList2.ionViewDidEnter();

        this.route.queryParams.subscribe((res) => {

            console.log('refreshZone', res);

            if (res.refreshZone) {

                if (this.settings.settings.selected_zone) {
                    this.storageService.setItem('selected_zone', this.settings.settings.selected_zone);

                    this.processZone(this.settings.settings.selected_zone.code);

                    // this.getAllPrayer(this.settings.settings.selected_zone.code);
                }
            }

            if (res.refresh) {
                this.getNews();
            }
        });

        this.storageService.getItem('language').then(language => {
            this.language = language;
        });
    }

    ionViewDidLeave() {
        this.horizontalCardList.ionViewDidLeave();
        this.horizontalCardList2.ionViewDidLeave();
    }

    ngOnInit() {

        this.platform.ready().then(() => {
            this.nativeStorage.getItem('blocks').then(data => {
                this.data.blocks = data.blocks;
                this.data.categories = data.categories;
                this.data.mainCategories = this.data.categories.filter(item => item.parent == 0);
                this.settings.pages = this.data.blocks.pages;
                this.settings.settings = this.data.blocks.settings;
                this.settings.dimensions = this.data.blocks.dimensions;
                this.settings.currency = this.data.blocks.settings.currency;

                if (this.data.blocks.languages) {
                    this.settings.languages = Object.keys(this.data.blocks.languages).map(i => this.data.blocks.languages[i]);
                }
                this.settings.currencies = this.data.blocks.currencies;
                this.settings.calc(this.platform.width());
                if (this.settings.colWidthLatest == 4) { this.filter.per_page = 15; }
                // this.settings.theme = this.data.blocks.theme;
                this.splashScreen.hide();
            }, error => console.error(error));

            this.nativeStorage.getItem('settings').then(data => {
                if (data.lang) {
                    this.config.lang = data.lang;
                    this.translateService.setDefaultLang(data.lang);
                    if (data.lang == 'ar') {
                        document.documentElement.setAttribute('dir', 'rtl');
                    }
                }
            }, error => console.error(error));

            this.getInfaqTypes();
            this.getBlocks();
        });

        this.storageService.getItem('language_id').then(language_id => {
            this.config.lang_id = language_id;
        });

        setTimeout(() => {
            this.getNews();
        }, 2000);
    }

    getAllPrayer(zone) {

        console.log('getAllPrayer', zone);

        const cacheKey = zone + '_' + new Date().getFullYear();

        this.nativeStorage.getItem(cacheKey).then(takwimsolat => {

            if (takwimsolat) {
                this.processPrayerTimes(takwimsolat);
            } else {
                this.prayerApi(zone);
            }

        }, error => {
            console.log('error getting from cache');
            this.prayerApi(zone);
        });

    }

    prayerApi(zone) {

        console.log('prayerApi', zone);

        this.solatService.getAllPrayer(zone).subscribe(result => {

            const data: any = result;

            if (result && data.prayerTime) {
                this.processPrayerTimes(result);

                const cacheKey = zone + '_' + new Date().getFullYear();

                this.nativeStorage.setItem(cacheKey, result).then(
                    () => console.log('Stored item!'), error => console.error('Error storing prayer time API', error));
            }

        });
    }

    processPrayerTimes(result) {

        // this.solatService.setNotifications(result);

        // const data: any = result;
        // const tempPrayerTime = this.solatService.getNextPrayer(data.prayerTime);

        // console.log('tempPrayerTime', tempPrayerTime);
        // this.prayerTime.currentPrayerTime = tempPrayerTime;

        // this.prayerTime.time = moment(this.prayerTime.currentPrayerTime.timeStamp, 'HH:mm:ss').format('h:mm');
        // this.prayerTime.ampm = moment(this.prayerTime.currentPrayerTime.timeStamp, 'HH:mm:ss').format('A');

        this.settings.settings.prayerTimes = this.solatService.getAllPrayerTimes(result);

        if (this.platform.is('desktop')) {
            this.storage.set('prayer_times', this.settings.settings.prayerTimes);
        } else {
            this.nativeStorage.setItem('prayer_times', this.settings.settings.prayerTimes).then(
                () => console.log('Stored prayer_times!'), error => { });
        }

    }

    setNextPrayer(selectedZone) {

        // reset
        this.prayerTime.time = '';
        this.prayerTime.ampm = '';

        const today = new Date();
        const tomorrow = moment().add(1, 'days').toDate();

        if (selectedZone) {
            this.solatService.getFirebasePrayer(today, selectedZone).subscribe(prayerTimeToday => {

                this.solatService.getFirebasePrayer(tomorrow, selectedZone).subscribe(prayerTimeTomorrow => {

                    const nextPrayer = this.solatService.getFirebaseNextPrayer(prayerTimeToday, prayerTimeTomorrow);

                    this.prayerTime.currentPrayerTime = nextPrayer;
                    this.prayerTime.time = moment(this.prayerTime.currentPrayerTime.timeStamp, 'HH:mm:ss').format('h:mm');
                    this.prayerTime.ampm = moment(this.prayerTime.currentPrayerTime.timeStamp, 'HH:mm:ss').format('A');

                });

            });
        }

    }

    processZone(zone) {

        // please disable this on live
        // if (this.settings.settings.zone_list) {
        //     this.settings.settings.zone_list.forEach(zones => {

        //         zones.data.forEach(zonex => {
        //             this.setNextPrayer(zonex.code);
        //         });

        //     });
        // }

        this.setNextPrayer(zone);
        this.solatService.setFirebaseNotifications(zone);

    }

    getZone() {

        this.storageService.getItem('selected_zone').then(selectedZone => {
            if (selectedZone) {
                this.settings.settings.selected_zone = selectedZone;

                this.processZone(selectedZone.code);

                this.getAllPrayer(this.settings.settings.selected_zone.code);
            }
        }, error => {

            console.log('selected_zone not found', error);

            this.nativeStorage.getItem('not_first_time').then(data => {

            }, error2 => {
                this.navCtrl.navigateForward('/tabs/home/zone');
            });
        });

    }

    getCart() {
        this.api.postItem('cart').subscribe(res => {
            this.cart = res;
            this.data.updateCart(this.cart.cart_contents);
            this.data.cartNonce = this.cart.cart_nonce;
        }, err => {
            console.log(err);
        });
    }

    getBlocks() {

        this.getZone();

        if (this.platform.is('desktop')) {
            return;
        }

        this.api.postItem('keys').subscribe(res => {

            this.data.blocks = res;

            if (this.data.blocks.popup.image) {
                this.presentPopup(this.data.blocks.popup.image.image);
            }

            if (this.data.blocks.user) {
                this.settings.user = this.data.blocks.user.data;
            }
            // this.settings.theme = this.data.blocks.theme;
            this.settings.pages = this.data.blocks.pages;
            if (this.data.blocks.user) {
                this.settings.reward = this.data.blocks.user.data.points_vlaue;
            }
            if (this.data.blocks.languages) {
                this.settings.languages = Object.keys(this.data.blocks.languages).map(i => this.data.blocks.languages[i]);
            }
            this.settings.currencies = this.data.blocks.currencies;
            this.settings.settings = this.data.blocks.settings;

            // console.log('this.settings.settings', this.settings.settings);

            // check app version
            if (this.settings.settings.app_version <= this.data.appVersion) {
                console.log('version ok');
            } else {
                console.log('version not ok');
                this.presentAlertUpdate();
            }

            this.settings.dimensions = this.data.blocks.dimensions;
            this.settings.currency = this.data.blocks.settings.currency;

            if (this.data.blocks.categories) {
                this.data.categories = this.data.blocks.categories.filter(item => item.name != 'Uncategorized');
                this.data.mainCategories = this.data.categories.filter(item => item.parent == 0);
            }
            this.settings.calc(this.platform.width());
            if (this.settings.colWidthLatest == 4) { this.filter.per_page = 15; }
            this.splashScreen.hide();
            this.getCart();
            this.processOnsignal();
            if (this.data.blocks.user) {
                this.settings.customer = this.data.blocks.user.data;
                this.settings.customer.id = this.data.blocks.user.ID;
                // tslint:disable-next-line: max-line-length
                if (this.data.blocks.user.allcaps.dc_vendor || this.data.blocks.user.allcaps.seller || this.data.blocks.user.allcaps.wcfm_vendor) {
                    this.settings.vendor = true;
                }
            }
            for (const item in this.data.blocks.blocks) {
                if (this.data.blocks.blocks[item].block_type == 'flash_sale_block') {
                    this.data.blocks.blocks[item].interval = setInterval(() => {
                        const countDownDate = new Date(this.data.blocks.blocks[item].sale_ends).getTime();
                        const now = new Date().getTime();
                        const distance = countDownDate - now;
                        this.data.blocks.blocks[item].days = Math.floor(distance / (1000 * 60 * 60 * 24));
                        this.data.blocks.blocks[item].hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
                        this.data.blocks.blocks[item].minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
                        this.data.blocks.blocks[item].seconds = Math.floor((distance % (1000 * 60)) / 1000);
                        if (distance < 0) {
                            clearInterval(this.data.blocks.blocks[item].interval);
                            this.data.blocks.blocks[item].hide = true;
                        }
                    }, 1000);
                }
            }
            if (this.data.blocks.settings.show_latest) {
                this.data.products = this.data.blocks.recentProducts;
            }
            if (this.data.blocks.user) {
                this.api.postItem('get_wishlist').subscribe(res => {
                    for (const item in res) {
                        this.settings.wishlist[res[item].id] = res[item].id;
                    }
                }, err => {
                    console.log(err);
                });
            }

            this.nativeStorage.setItem('blocks', {
                blocks: this.data.blocks,
                categories: this.data.categories
            }).then(
                () => console.log('Stored item!'), error => console.error('Error storing item', error));

            /* Product Addons */
            if (this.data.blocks.settings.switchAddons) {
                this.api.getAddonsList('product-add-ons').subscribe(res => {
                    this.settings.addons = res;
                });
            }

            // get prefered zone
            this.getZone();

        }, err => {
            console.log(err);
        });
    }

    async presentAlertUpdate() {
        const alert = await this.alertController.create({
            header: 'Application is not up to date!',
            message: 'Please update the app',
            backdropDismiss: false,
            // buttons: [
            //     {
            //         text: 'Okay',
            //         handler: () => {
            //             console.log('Confirm Okay');
            //         }
            //     }
            // ]
        });

        await alert.present();
    }

    getInfaqTypes() {

        const params = {
            parent: 29,
            order: 'asc',
            orderby: 'description',
            hide_empty: false
        };

        if (this.platform.is('hybrid')) {
            this.api.getItemIonic('products/categories', params).then((res) => {

                if (res) {
                    this.processInfaq(res);
                }

            }, err => {
                console.log(err);
            });
        } else {
            this.api.getItem('products/categories', params).subscribe(res => {

                if (res) {
                    this.processInfaq(res);
                }

            }, err => {
                console.log(err);
            });
        }
    }

    processInfaq(res) {

        const infaqTypes: any = res;

        infaqTypes.forEach(infaq => {
            if (!infaq.image) {
                infaq.image_path = '';
            } else {
                infaq.image_path = infaq.image.src;
            }
        });

        this.data.updateInfaqTypes(infaqTypes);

        this.nativeStorage.setItem('infaqTypes', infaqTypes).then(
            () => console.log('Stored item!'), error => console.error('Error storing item', error));
    }

    goto(item) {
        if (item.description == 'category') {
            this.navCtrl.navigateForward('/tabs/home/products/' + item.url);
        } else if (item.description == 'product') {
            this.navCtrl.navigateForward('/tabs/home/product/' + item.url);
        } else if (item.description == 'post') {
            this.navCtrl.navigateForward('/tabs/home/post/' + item.url);
        }
    }

    getProduct(item) {
        this.product.product = item;
        this.navCtrl.navigateForward('/tabs/home/product/' + item.id);
    }

    getSubCategories(id) {
        const results = this.data.categories.filter(item => item.parent === parseInt(id));
        return results;
    }

    getCategory(id) {
        this.navCtrl.navigateForward('/tabs/home/products/' + id);
    }

    loadData(event) {
        this.filter.page = this.filter.page + 1;
        this.api.postFlutterItem('products', this.filter).subscribe(res => {
            this.tempProducts = res;
            this.data.products.push.apply(this.data.products, this.tempProducts);
            event.target.complete();
            if (this.tempProducts.length == 0) { this.hasMoreItems = false; }
        }, err => {
            event.target.complete();
        });
    }

    processOnsignal() {
        this.oneSignal.startInit(this.data.blocks.settings.onesignal_app_id, this.data.blocks.settings.google_project_id);
        // this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.InAppAlert);
        this.oneSignal.handleNotificationReceived().subscribe(() => {
            // do something when notification is received
        });
        this.oneSignal.handleNotificationOpened().subscribe(result => {
            if (result.notification.payload.additionalData.category) {
                this.navCtrl.navigateForward('/tabs/home/products/' + result.notification.payload.additionalData.category);
            } else if (result.notification.payload.additionalData.product) {
                this.navCtrl.navigateForward('/tabs/home/product/' + result.notification.payload.additionalData.product);
            } else if (result.notification.payload.additionalData.post) {
                this.navCtrl.navigateForward('/tabs/home/post/' + result.notification.payload.additionalData.post);
            } else if (result.notification.payload.additionalData.order) {
                this.navCtrl.navigateForward('/tabs/account/orders/order/' + result.notification.payload.additionalData.order);
            }
        });
        this.oneSignal.endInit();
    }

    doRefresh(event) {
        this.filter.page = 1;
        this.getInfaqTypes();
        this.getBlocks();
        this.highlights = [];
        this.featureds = [];
        this.getNews();
        setTimeout(() => {
            event.target.complete();
        }, 1500);
    }

    getHeight(child) {
        return (child.height * this.screenWidth) / child.width;
    }

    async getNews() {

        const paramsHighlight = {
            _embed: true,
            per_page: 4,
            categories: [16, this.config.lang_id],
            cat_relation: 'AND'
        };
        const paramsFeatured = {
            _embed: true,
            categories: [17, this.config.lang_id],
            cat_relation: 'AND'
        };

        if (this.platform.is('hybrid')) {
            await this.api.getWPHybrid('posts', paramsHighlight).then((res) => {
                if (res) {
                    this.highlights = res;
                    this.processNews(this.highlights);
                }
            }, err => {
                console.log(err);
            });

            await this.api.getWPHybrid('posts', paramsFeatured).then((res) => {
                if (res) {
                    this.featureds = res;
                    this.processNews(this.featureds);
                }
            }, err => {
                console.log(err);
            });
        } else {
            await this.api.getWP('posts', paramsHighlight).subscribe(res => {
                if (res) {
                    this.highlights = res;
                    this.processNews(this.highlights);
                }
            }, err => {
                console.log(err);
            });
            await this.api.getWP('posts', paramsFeatured).subscribe(res => {
                if (res) {
                    this.featureds = res;
                    this.processNews(this.featureds);
                }
            }, err => {
                console.log(err);
            });
        }

    }

    processNews(news) {
        news.forEach(highlight => {
            if (highlight._embedded && highlight._embedded['wp:featuredmedia']) {
                highlight.media = highlight._embedded['wp:featuredmedia'][0].media_details.sizes.medium_large.source_url;
            } else {
                highlight.media = '/assets/no-image.jpg';
            }
        });

        if (news.length <= 0) {
            news[0] = {
                title: {
                    rendered: '...'
                },
                media: ''
            };
        }
    }

    async presentPopup(imageSrc) {
        const popup = await this.alertController.create({
            mode: 'md',
            cssClass: 'alert-popup',
            buttons: [
                {
                    text: 'X',
                    role: 'cancel',
                    cssClass: 'btn-close',
                    handler: () => {
                        console.log('Confirm Cancel');
                    }
                }
            ],
            message: '<div><img src="' +
                imageSrc +
                '" class="card-alert"></div>'
        });

        // <ion-button (click)="dismissPopup()" color="light" size="small" class="btn-close"><ion-icon slot="icon-only" name="close"></ion-icon></ion-button>

        await popup.present();
    }
}
