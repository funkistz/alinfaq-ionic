import { Component } from '@angular/core';
import { LoadingController, NavController, ToastController, Platform } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { NativeStorage } from '@ionic-native/native-storage/ngx';
import { Settings } from '../data/settings';
import * as moment from 'moment';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';
import { StorageService } from '../services/storage/storage.service';
import { FirebaseService } from '../services/firebase.service';

@Component({
    selector: 'app-prayer',
    templateUrl: 'prayer.page.html',
    styleUrls: ['prayer.page.scss']
})
export class PrayerPage {

    prayerTimes;
    prayerToday;

    months = [
        {
            name: 'January',
            filter: '01',
        },
        {
            name: 'February',
            filter: '02',
        },
        {
            name: 'March',
            filter: '03',
        },
        {
            name: 'April',
            filter: '04',
        },
        {
            name: 'May',
            filter: '05',
        },
        {
            name: 'June',
            filter: '06',
        },
        {
            name: 'July',
            filter: '07',
        },
        {
            name: 'August',
            filter: '08',
        },
        {
            name: 'September',
            filter: '09',
        },
        {
            name: 'October',
            filter: '10',
        },
        {
            name: 'November',
            filter: '11',
        },
        {
            name: 'December',
            filter: '12',
        },
    ];

    constructor(
        public platform: Platform,
        private nativeStorage: NativeStorage,
        private storage: Storage,
        public settings: Settings,
        public navCtrl: NavController,
        private storageService: StorageService,
        private firebaseService: FirebaseService,
    ) {

        // console.log(this.settings.settings.prayerTimes);

        this.storageService.getItem('selected_zone').then(zone => {

            this.firebaseService.getAllPrayerTimes(zone.code).subscribe(prayerTimes => {
                this.prayerTimes = prayerTimes;
            });

            const date = new Date();
            this.firebaseService.getFirebasePrayer(date, zone.code).subscribe(prayerTime => {

                console.log('getFirebasePrayerToday', prayerTime);
                this.prayerToday = prayerTime;
            });

        });

        // this.storageService.getItem('prayer_times').then(prayerTimes => {
        //     if (prayerTimes && prayerTimes.prayerTime) {
        //         this.prayerTimes = prayerTimes;

        //         // this.prayerToday = this.prayerTimes.prayerTime.find(prayer => {
        //         //     return prayer.date === moment().format('YYYY-MM-DD');
        //         // });

        //         this.prayerToday = this.prayerTimes.prayerTime.filter(prayer => {
        //             return prayer.date === moment().format('YYYY-MM-DD');
        //         })[0];

        //         console.log(this.prayerToday);
        //     }
        // });

    }

    viewPrayerByMonth(month) {

        const year = moment().format('YYYY');

        const prayerByMonth = this.prayerTimes.filter(prayer => {
            return prayer.date.includes(year + '-' + month.filter);
        });

        // console.log('prayerByMonth', JSON.stringify(prayerByMonth[0]));


        const navigationExtras: NavigationExtras = {
            queryParams: {
                prayerTimes: prayerByMonth,
                month
            }
        };
        this.navCtrl.navigateForward('/tabs/more/prayer/' + month.name, navigationExtras);

    }
}
