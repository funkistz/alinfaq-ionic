import { Component, OnInit } from '@angular/core';
import { NavController, ToastController, LoadingController, Platform } from '@ionic/angular';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from '../api.service';
import { Settings } from '../data/settings';
import { NativeStorage } from '@ionic-native/native-storage/ngx';

@Component({
    selector: 'app-infaq-type',
    templateUrl: 'infaq-type.page.html',
    styleUrls: ['infaq-type.page.scss']
})

export class InfaqTypePage {

    loading: any;
    infaqTypes = [
        {
            id: 1,
            name: 'Goods',
            icon: '/assets/icon/goods.png',
            col: 6
        },
        {
            id: 2,
            name: 'Cash',
            icon: '/assets/icon/cash.png',
            col: 6
        },
        {
            id: 3,
            name: 'Goods & Cash',
            icon: '/assets/icon/cashngoods.png',
            col: 12
        }
    ];

    infaq: any;
    country: any;
    cashProduct: any;
    extraCountry: any;

    constructor(
        public api: ApiService,
        public router: Router,
        public navCtrl: NavController,
        public settings: Settings,
        public route: ActivatedRoute,
        public toastController: ToastController,
        public loadingController: LoadingController,
        private nativeStorage: NativeStorage,
        public platform: Platform,
    ) {

        this.route.queryParams.subscribe((res) => {
            this.infaq = res.infaq;
            this.country = res.country;
            this.extraCountry = res.extra_country;
        });

        const slugname = 'cash-infaq';
        // this.getCashProduct(slugname);
    }

    async getCashProduct(slugName) {

        console.log('get cash:' + slugName);

        const params = {
            slug: slugName,
            per_page: 1,
        };

        if (this.platform.is('hybrid')) {
            await this.api.getItemIonic('products', params).then((res) => {
                if (res) {
                    this.cashProduct = res[0];
                    this.nativeStorage.setItem('cashProduct', res[0]).then(
                        () => console.log('Stored item!'), error => console.error('Error storing item', error));
                }
            }, err => {
                console.log(err);
            });
        } else {
            await this.api.getItem('products', params).subscribe(res => {
                if (res[0]) {
                    this.cashProduct = res[0];
                    this.nativeStorage.setItem('cashProduct', res[0]).then(
                        () => console.log('Stored item!'), error => console.error('Error storing item', error));
                }
            }, err => {
                console.log(err);
            });
        }

    }

    selectInfaqType(infaqType) {

        if (infaqType.id === 1) {

            this.redirectInfaq();

        } else if (infaqType.id === 2) {

            const params = {
                infaq_id: this.infaq.id,
                infaq_name: this.infaq.name,
                country_id: this.country.id,
                country_name: this.country.name,
                extra_country: this.extraCountry,
            };

            this.router.navigate(['/tabs/home/infaq/cash/' + this.settings.settings.cash_infaq], {
                queryParams: params,
            });
        } else if (infaqType.id === 3) {

            this.redirectInfaq(true);
        }

    }

    redirectInfaq(isBoth = false) {

        const params: any = {
            infaq_id: this.infaq.id,
            infaq_name: this.infaq.name,
            country_id: this.country.id,
            country_name: this.country.name,
            country_src: this.country.image_path,
            extra_country: this.extraCountry,
        };

        if (isBoth) {
            params.cash_id = this.settings.settings.cash_infaq;
        }

        this.router.navigate(['/tabs/home/infaq/categories'], {
            queryParams: params,
        });
    }

    ngOnInit() {

    }

}
