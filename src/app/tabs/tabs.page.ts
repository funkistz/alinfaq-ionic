import { Component } from '@angular/core';
import { Data } from '../data';
import { Settings } from '../data/settings';
import { Router } from '@angular/router';

@Component({
  selector: 'app-tabs',
  templateUrl: 'tabs.page.html',
  styleUrls: ['tabs.page.scss']
})
export class TabsPage {
  constructor(
    public data: Data,
    public settings: Settings,
    private router: Router
  ) {

    // console.log('data', data);
  }

  tabChanged($event) {
    // $ev.setRoot($ev.root);
    // console.log('tabs', $event.tab);

    if ($event.tab === 'cart') {
      this.router.navigateByUrl('/tabs/cart');
    }
  }

}
