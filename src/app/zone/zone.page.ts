import { Component, OnInit } from '@angular/core';
import { NavController, Platform } from '@ionic/angular';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from '../api.service';
import { Settings } from '../data/settings';
import { AlertController } from '@ionic/angular';
import { NativeStorage } from '@ionic-native/native-storage/ngx';
import { Storage } from '@ionic/storage';

@Component({
    selector: 'app-zone',
    templateUrl: 'zone.page.html',
    styleUrls: ['zone.page.scss']
})
export class ZonePage {

    id: any;
    title: any = '';
    catId: any;
    post: any;
    country: any;
    noTitle = false;
    productId: any;
    zoneLists: any;
    zone: any;
    zoneNames: any;

    constructor(
        public api: ApiService,
        public router: Router,
        public navCtrl: NavController,
        public settings: Settings,
        public route: ActivatedRoute,
        public platform: Platform,
        public alertController: AlertController,
        private nativeStorage: NativeStorage,
        private storage: Storage
    ) {

    }

    ionViewDidEnter() {

        if (this.platform.is('desktop')) {
            this.storage.set('not_first_time', new Date().getTime());
        } else {
            this.nativeStorage.setItem('not_first_time', new Date().getTime()).then(
                () => console.log('not first time'), error => console.error('Error not first time', error));
        }

        this.zoneLists = this.settings.settings.zone_list;
    }

    selectZone(zone) {

        this.zone = zone;
        console.log(this.zone);

    }

    chooseZone() {

        const zoneName = this.zone.zone.split(',');
        const zoneNames = [];

        console.log(zoneName.length);

        if (zoneName.length > 1) {

            zoneName.forEach(zone => {

                zoneNames.push({
                    name: 'zoneName',
                    type: 'radio',
                    label: zone,
                    value: zone,
                });

            });
            this.zoneNames = zoneNames;

            this.presentAlertRadio();
        } else {
            this.confirmZone(this.zone.zone);
        }

    }

    confirmZone(zoneName) {

        this.settings.settings.selected_zone = this.zone;
        this.settings.settings.selected_zone.display_name = zoneName;

        const params = {
            refreshZone: true
        };

        this.router.navigate(['/tabs/home'], {
            queryParams: params,
        });

    }

    async presentAlertRadio() {
        const alert = await this.alertController.create({
            header: 'Choose Zone',
            inputs: this.zoneNames,
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                    cssClass: 'secondary',
                    handler: () => {
                        console.log('Confirm Cancel');
                    }
                }, {
                    text: 'Ok',
                    handler: (data) => {

                        this.confirmZone(data);
                        console.log('Confirm Ok', data);
                    }
                }
            ]
        });

        await alert.present();
    }
}
