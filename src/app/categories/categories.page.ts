import { Component } from '@angular/core';
import { LoadingController, NavController, Platform } from '@ionic/angular';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from '../api.service';
import { Data } from '../data';
import { Settings } from '../data/settings';

@Component({
    selector: 'app-categories',
    templateUrl: 'categories.page.html',
    styleUrls: ['categories.page.scss']
})
export class CategoriesPage {

    loader = false;
    categories: any;
    country: any = {};
    infaq: any = {};
    country_name = '';
    res: any;
    extraCountry: any;

    constructor(
        public api: ApiService,
        public data: Data,
        public loadingController: LoadingController,
        public navCtrl: NavController,
        public router: Router,
        public settings: Settings,
        public route: ActivatedRoute,
        public platform: Platform,
    ) {
        this.getInfaqCategories();
        this.route.queryParams.subscribe((res) => {
            this.country.id = res.country_id;
            this.country.name = res.country_name;
            this.country.src = res.country_src;
            this.infaq.id = res.infaq_id;
            this.infaq.name = res.infaq_name;
            this.extraCountry = res.extra_country;
            this.res = res;
        });
    }

    ionViewWillEnter() {

    }

    async getInfaqCategories() {

        this.loader = true;
        // 37 = infaq type parent
        const params = {
            parent: 37,
            hide_empty: true,
            order: 'asc',
            orderby: 'description',
        };

        if (this.platform.is('hybrid')) {
            await this.api.getItemIonic('products/categories', params).then((res) => {
                this.loader = false;
                if (res) {
                    this.processCategories(res);
                }
            }, err => {
                console.log(err);
                this.loader = false;
            });
        } else {
            await this.api.getItem('products/categories', params).subscribe(res => {
                this.loader = false;
                if (res) {
                    this.processCategories(res);
                }
            }, err => {
                console.log(err);
                this.loader = false;
            });
        }

    }

    processCategories(res) {
        this.categories = res;
        this.categories.forEach(cat => {
            if (!cat.image) {
                cat.image_path = '';
            } else {
                cat.image_path = cat.image.src;
            }
        });
    }

    getProducts(id) {

        const params: any = {
            infaq_id: this.infaq.id,
            infaq_name: this.infaq.name,
            country_id: this.country.id,
            country_name: this.country.name,
            extra_country: this.res.extra_country,
        };

        if (this.res.cash_id) {
            params.cash_id = this.res.cash_id;
        }

        this.router.navigate(['/tabs/home/infaq/categories/products/' + id], {
            queryParams: params,
        });
    }

    subCategories(id) {
        return this.data.categories.filter(item => item.parent == id);
    }

    showSubCategory(i) {
        const intial = this.data.mainCategories[i].show;
        this.data.mainCategories.forEach(item => item.show = false);
        this.data.mainCategories[i].show = !intial;
        if (this.data.categories.filter(item => item.parent == this.data.mainCategories[i].id).length == 0) {
            this.getProducts(this.data.mainCategories[i].id);
        }
    }
}
