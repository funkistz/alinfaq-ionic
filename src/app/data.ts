import { Injectable } from '@angular/core';
@Injectable({
  providedIn: 'root'
})
export class Data {

  appVersion: any = '3.001';

  categories: any = [];
  mainCategories: any = [];
  blocks: any = [];
  cart: any = {};
  count = 0;
  cartItem: any;
  wishlistId: any = [];
  freaturedProducts: any = [];
  onsaleProducts: any = [];
  products: any = [];
  cartNonce: any = '';
  global: any = {};
  infaqTypes: any = [];
  popup: any = [];

  constructor() { }

  updateCart(cart_contents) {
    this.cartItem = cart_contents;
    this.cart = [];
    this.count = 0;
    for (const item in cart_contents) {
      this.cart[cart_contents[item].product_id] = parseInt(cart_contents[item].quantity);
      // this.count += parseInt(cart_contents[item].quantity);
      this.count++;
    }
  }

  updateInfaqTypes(infaqTypes) {
    this.infaqTypes = infaqTypes;
  }

  getInfaqType(id) {
    return this.infaqTypes.find(obj => {
      return obj.id === Number(id);
    });
  }
}

