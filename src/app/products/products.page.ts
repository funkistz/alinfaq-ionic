import { Component, OnInit, ViewChild } from '@angular/core';
import { NavController, ModalController } from '@ionic/angular';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from '../api.service';
import { Data } from '../data';
import { Settings } from '../data/settings';
import { Product } from '../data/product';
import { FilterPage } from '../filter/filter.page';
import { Vendor } from '../data/vendor';
import { TranslateService } from '@ngx-translate/core';
import { AlertController } from '@ionic/angular';
import { Config } from '../config';
import { HttpParams } from '@angular/common/http';

@Component({
    selector: 'app-products',
    templateUrl: 'products.page.html',
    styleUrls: ['products.page.scss']
})
export class ProductsPage {

    title = '';
    country_name = '';
    products: any = [];
    tempProducts: any = [];
    subCategories: any = [];
    filter: any = {};
    attributes: any;
    hasMoreItems = true;
    loader = false;
    searchInput: any;
    showSearch = true;

    cart: any = {};
    options: any = {};
    lan: any = {};
    variationId: any;
    gridView = true;

    res: any;
    infaq: any = {};

    constructor(
        public config: Config,
        public alertController: AlertController,
        public translate: TranslateService,
        public vendor: Vendor,
        public modalController: ModalController,
        public api: ApiService,
        public data: Data,
        public product: Product,
        public settings: Settings,
        public router: Router,
        public navCtrl: NavController,
        public route: ActivatedRoute
    ) {
        this.filter.page = 1;
        this.filter.status = 'publish';
        this.options.quantity = 1;

        this.route.queryParams.subscribe((res) => {
            this.filter.country_id = res.country_id;
            this.title = res.infaq_name;
            this.res = res;
            this.infaq.id = res.infaq_id;
            this.infaq.name = res.infaq_name;
        });
    }

    ionViewWillEnter() {

    }

    async getFilter() {
        const modal = await this.modalController.create({
            component: FilterPage,
            componentProps: {
                filter: this.filter,
                attributes: this.attributes
            }
        });
        modal.present();
        const {
            data
        } = await modal.onDidDismiss();
        if (data) {
            this.filter = data;
            Object.keys(this.filter).forEach(key => this.filter[key] === undefined ? delete this.filter[key] : '');
            this.filter.page = 1;
            this.getProducts();
        }
    }

    loadData(event) {
        this.filter.page = this.filter.page + 1;
        this.api.postFlutterItem('products', this.filter).subscribe(res => {
            this.tempProducts = res;
            this.products.push.apply(this.products, this.tempProducts);
            event.target.complete();
            if (this.tempProducts.length == 0) { this.hasMoreItems = false; }
        }, err => {
            event.target.complete();
        });
        console.log('Done');
    }

    getProducts() {
        this.loader = true;
        this.api.postFlutterItem('products', this.filter).subscribe(res => {
            console.log(res);
            this.products = res;
            this.loader = false;
        }, err => {
            console.log(err);
        });
    }

    getAttributes() {
        this.api.postItem('product-attributes', {
            category: this.filter.id
        }).subscribe(res => {
            this.attributes = res;
        }, err => {
            console.log(err);
        });
    }

    ngOnInit() {
        if (this.route.snapshot.paramMap.get('id')) {
            this.filter.id = this.route.snapshot.paramMap.get('id');
        }
        if (this.vendor.vendor) {
            this.filter.vendor = this.vendor.vendor.id ? this.vendor.vendor.id : this.vendor.vendor.ID;
        }

        if (this.data.categories && this.data.categories.length) {
            for (let i = 0; i < this.data.categories.length; i++) {
                if (this.data.categories[i].parent == this.filter.id) {
                    this.subCategories.push(this.data.categories[i]);
                }
            }
        }
        if (this.settings.colWidthProducts == 4) { this.filter.per_page = 15; }

        this.getProducts();
        this.getAttributes();

        this.translate.get(['Oops!', 'Please Select', 'Please wait', 'Options', 'Option', 'Select', 'Item added to cart', 'Message', 'Requested quantity not available']).subscribe(translations => {
            this.lan.oops = translations['Oops!'];
            this.lan.PleaseSelect = translations['Please Select'];
            this.lan.Pleasewait = translations['Please wait'];
            this.lan.options = translations.Options;
            this.lan.option = translations.Option;
            this.lan.select = translations.Select;
            this.lan.addToCart = translations['Item added to cart'];
            this.lan.message = translations.Message;
            this.lan.lowQuantity = translations['Requested quantity not available'];
        });
    }

    getProduct(product) {
        this.product.product = product;
        // this.navCtrl.navigateForward(this.router.url + '/product/' + product.id);
        // console.log('redirect product', 'tabs/infaq/products/' + this.filter.id + '/product/' + product.id);
        // this.navCtrl.navigateForward('tabs/infaq/products/' + this.filter.id + '/product/' + product.id);

        const params: any = {
            infaq_id: this.res.infaq_id,
            infaq_name: this.res.infaq_name,
            country_id: this.res.country_id,
            country_name: this.res.country_name,
            extra_country: this.res.extra_country,
        };

        if (this.res.cash_id) {
            params.cash_id = this.res.cash_id;
        }

        this.router.navigate(['tabs/home/infaq/products/' + this.filter.id + '/product/' + product.id], {
            queryParams: params,
        });
    }

    getCategory(id) {
        const endIndex = this.router.url.lastIndexOf('/');
        const path = this.router.url.substring(0, endIndex);
        this.navCtrl.navigateForward(path + '/' + id);
    }

    loaded(product) {
        console.log('Loaded');
        product.loaded = true;
    }

    onInput() {
        if (this.searchInput.length) {
            this.products = '';
            this.filter.q = this.searchInput;
            this.filter.page = 1;
            this.getProducts();
        } else {
            this.products = '';
            this.filter.q = undefined;
            this.filter.page = 1;
            this.getProducts();
        }
    }

    ionViewWillLeave() {
        this.showSearch = false;
    }
    ionViewDidLeave() {
        this.vendor.vendor = {};
        this.showSearch = true;
    }
    toggleGridView() {
        this.gridView = !this.gridView;
    }

    async addToCart(product) {
        if (product.manage_stock && product.stock_quantity < this.data.cart[product.id]) {
            this.presentAlert(this.lan.message, this.lan.lowQuantity);
        } else if (product.type == 'variable') {
            this.getProduct(product);
        } else if (this.setVariations(product)) {

            if (this.data.cart[product.id] != undefined) { this.data.cart[product.id] += 1; } else { this.data.cart[product.id] = 1; }

            this.options.product_id = product.id;
            await this.api.postItem('add_to_cart', this.options).subscribe(res => {
                this.cart = res;
                this.data.updateCart(this.cart.cart);
            }, err => {
                console.log(err);
            });
        }
    }

    setVariations(product) {
        if (product.variationId) {
            this.options.variation_id = product.variationId;
        }
        product.attributes.forEach(item => {
            if (item.selected) {
                this.options['variation[attribute_pa_' + item.name + ']'] = item.selected;
            }
        });
        for (let i = 0; i < product.attributes.length; i++) {
            if (product.type == 'variable' && product.attributes[i].variation && product.attributes[i].selected == undefined) {
                this.presentAlert(this.lan.options, this.lan.select + ' ' + product.attributes[i].name + ' ' + this.lan.option);
                return false;
            }
        }
        return true;
    }

    async presentAlert(header, message) {
        const alert = await this.alertController.create({
            header,
            message,
            buttons: ['OK']
        });
        await alert.present();
    }

    async updateToCart(product) {

        if (product.manage_stock && product.stock_quantity < this.data.cart[product.id]) {
            this.presentAlert(this.lan.message, this.lan.lowQuantity);
        } else {

            let params = new HttpParams();

            for (const key in this.data.cartItem) {
                if (this.data.cartItem[key].product_id == product.id) {
                    if (this.data.cartItem[key].quantity != undefined && this.data.cartItem[key].quantity == 0) {
                        this.data.cartItem[key].quantity = 0;
                    } else {
                        this.data.cartItem[key].quantity += 1;
                    }
                    if (this.data.cart[product.id] != undefined && this.data.cart[product.id] == 0) {
                        this.data.cart[product.id] = 0;
                    } else {
                        this.data.cart[product.id] += 1;
                    }
                    params = params.set('cart[' + key + '][qty]', this.data.cartItem[key].quantity);
                }
            }

            params = params.set('_wpnonce', this.data.cartNonce);
            params = params.set('_wp_http_referer', this.config.url + '/wp-admin/admin-ajax.php?action=mstoreapp-cart');
            params = params.set('update_cart', 'Update Cart');

            await this.api.updateCart('/cart/', params).subscribe(res => {
                console.log(res);
                this.cart = res;
                this.data.updateCart(this.cart.cart_contents);
            }, err => {
                console.log(err);
            });

        }

    }

    async deleteFromCart(product) {

        let params = new HttpParams();

        for (const key in this.data.cartItem) {
            if (this.data.cartItem[key].product_id == product.id) {
                if (this.data.cartItem[key].quantity != undefined && this.data.cartItem[key].quantity == 0) {
                    this.data.cartItem[key].quantity = 0;
                } else {
                    this.data.cartItem[key].quantity -= 1;
                }
                if (this.data.cart[product.id] != undefined && this.data.cart[product.id] == 0) {
                    this.data.cart[product.id] = 0;
                } else {
                    this.data.cart[product.id] -= 1;
                }
                params = params.set('cart[' + key + '][qty]', this.data.cartItem[key].quantity);
            }
        }

        params = params.set('_wpnonce', this.data.cartNonce);
        params = params.set('_wp_http_referer', this.config.url + '/wp-admin/admin-ajax.php?action=mstoreapp-cart');
        params = params.set('update_cart', 'Update Cart');

        await this.api.updateCart('/cart/', params).subscribe(res => {
            console.log(res);
            this.cart = res;
            this.data.updateCart(this.cart.cart_contents);
        }, err => {
            console.log(err);
        });
    }
}
