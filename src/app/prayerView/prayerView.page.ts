import { Component } from '@angular/core';
import { LoadingController, NavController, ToastController, Platform } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { NativeStorage } from '@ionic-native/native-storage/ngx';
import { Settings } from '../data/settings';
import * as moment from 'moment';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';

@Component({
    selector: 'app-prayerview',
    templateUrl: 'prayerview.page.html',
    styleUrls: ['prayerview.page.scss']
})
export class PrayerViewPage {

    prayerTimes;
    month;

    constructor(
        public platform: Platform,
        private nativeStorage: NativeStorage,
        private storage: Storage,
        public settings: Settings,
        public route: ActivatedRoute,
    ) {

        this.route.queryParams.subscribe((res) => {

            this.prayerTimes = res.prayerTimes;
            this.month = res.month;
            // console.log(res);
        });

    }
}
