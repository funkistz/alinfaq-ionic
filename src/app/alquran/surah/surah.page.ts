import { Component, OnInit, ViewChild } from '@angular/core';
import { AlquranService } from '../../services/alquran/alquran.service';
import { ActivatedRoute, Router } from '@angular/router';
import { IonContent } from '@ionic/angular';
import { StorageService } from '../../services/storage/storage.service';

@Component({
  selector: 'app-surah',
  templateUrl: './surah.page.html',
  styleUrls: ['./surah.page.scss'],
})
export class SurahPage implements OnInit {

  @ViewChild(IonContent, {read: IonContent, static: false}) myContent: IonContent;

  last_scroll;
  surah;
  nextSurah;
  verseDone = false;
  verses = [];
  versesDummy = [
    '',
    '',
    '',
    '',
    '',
    '',
    '',
    '',
    '',
    '',
    '',
    '',
    '',
  ];

  constructor(
    private alquranService: AlquranService,
    private route: ActivatedRoute,
    private storageService: StorageService,
    public router: Router,
  ) { }

  ngOnInit() {

    // const surahID: string = this.route.snapshot.paramMap.get('id');
    this.route.queryParams.subscribe(params => {
      // const params: any = param;
      if (params && params.surah) {

        this.surah = JSON.parse(params.surah);
        this.last_scroll = params.last_scroll;

        this.storageService.getItem(this.surah.id + '_last_scroll').then( last_scroll => {
          console.log('last_scroll', last_scroll);
          this.last_scroll = last_scroll;

          this.getVerses(this.surah.id);
          this.getNextSurah(this.surah.id);
        }, error => {
          this.getVerses(this.surah.id);
          this.getNextSurah(this.surah.id);
        });

        this.storageService.setItem('alquran_last_surah', this.surah);        

      }
    });

  }

  getNextSurah(surah_id){

    this.storageService.getItem('alquran_surah').then(surahs => {

      let isNext = false;
      let searchFinish = false;
      surahs.forEach(surah => {

        if(searchFinish){
          return;
        }

        if(!isNext){

          if(surah.id === surah_id){
            isNext = true;
          }

        }else{
          searchFinish = true;
          this.nextSurah = surah;
          console.log('next surah', this.nextSurah);
        }

      });


    }, error => {
      this.getVerses(this.surah.id);
    });

  }

  scroll(ev){
    // console.log('scroll event scrollTop', ev.detail.scrollTop);
    // console.log('scroll event scrollBottom', ev.detail.scrollBottom);
    // console.log('scroll event detail', ev.detail);

    this.storageService.setItem('alquran_last_scroll', ev.detail.scrollTop);
    this.storageService.setItem(this.surah.id + '_last_scroll', ev.detail.scrollTop);

  }

  getVerses(surah_id) {

    this.alquranService.getVerseList(surah_id).subscribe(verses => {
      this.verses = verses;
      this.verseDone = true;
      console.log('this.verses', this.verses);

      if(this.last_scroll){

        setTimeout(()=>{ 
          console.log('scrolling',this.last_scroll);
          this.myContent.scrollToPoint(0, this.last_scroll, 500);
        }, 200);

      }

    }, err => {
      this.verseDone = true;
    });

  }

  goNextSurah(){

    if(!this.nextSurah){
      return;
    }

    this.surah = null;
    this.verses = [];
    this.verseDone = false;

    let data = {
      surah: JSON.stringify(this.nextSurah)
    };

    this.router.navigate(['/tabs/more/alquran/surah'], {
        queryParams: data,
    });


  }

}
