import { Component } from '@angular/core';
import { LoadingController, NavController, ToastController, Platform } from '@ionic/angular';
import { DeviceOrientation, DeviceOrientationCompassHeading } from '@ionic-native/device-orientation/ngx';
import { Geolocation, Geoposition } from '@ionic-native/geolocation/ngx';
import { Diagnostic } from '@ionic-native/diagnostic/ngx';
import { AlertController } from '@ionic/angular';
import { AlquranService } from '../services/alquran/alquran.service';
import { ActivatedRoute, Router } from '@angular/router';
import { StorageService } from '../services/storage/storage.service';

@Component({
    selector: 'app-alquran',
    templateUrl: 'alquran.page.html',
    styleUrls: ['alquran.page.scss']
})
export class AlquranPage {

    public data: DeviceOrientationCompassHeading = null;
    public currentLocation: Geoposition = null;
    // Initial Kaaba location that we've got from google maps
    // private kaabaLocation: { lat: number, lng: number } = { lat: 21.42276, lng: 39.8256687 };
    private kaabaLocation: { lat: number, lng: number } = { lat: 21.45, lng: -39.75 };
    // Initial Qibla Location
    public qiblaLocation = 0;
    locationOn = 'pending';
    orientationOn = 'pending';

    public alquranList = [];
    public alquranListFilter = [];
    loaded = false;
    alquran_last_surah;
    alquran_last_scroll;

    constructor(
        private deviceOrientation: DeviceOrientation,
        private geolocation: Geolocation,
        public platform: Platform,
        private diagnostic: Diagnostic,
        public alertController: AlertController,
        private navCtrl: NavController,
        private alquranService: AlquranService,
        public router: Router,
        private storageService: StorageService,
    ) {

        this.alquranService.getSurahList().subscribe(surahs => {
            this.alquranList = surahs;
            this.alquranListFilter = surahs;
            this.loaded = true;

            this.storageService.setItem('alquran_surah', surahs);

            console.log(surahs);
          }, err => {
            this.loaded = true;
          });;


    }

    async filterList(evt){

      console.log('searching...');

      this.alquranListFilter = this.alquranList;
      const searchTerm = evt.srcElement.value;

      if (!searchTerm) {
        return;
      }

      this.alquranListFilter = this.alquranListFilter.filter(surah => {
        if (surah.name && searchTerm) {
          return (surah.name.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1);
        }
      });

    }

    ionViewDidEnter() {

      this.storageService.getItem('alquran_last_surah').then(alquran_last_surah => {
          this.alquran_last_surah = alquran_last_surah;
      });
      this.storageService.getItem('alquran_last_scroll').then(alquran_last_scroll => {
          this.alquran_last_scroll = alquran_last_scroll;
      });


    }

    viewSurahPage(surah){

      let data = {
        surah: JSON.stringify(surah),
      };

      this.router.navigate(['/tabs/more/alquran/surah'], {
          queryParams: data,
      });

    }

    lastReadPosition(){

      let data = {
        surah: JSON.stringify(this.alquran_last_surah),
        last_scroll: this.alquran_last_scroll
      };

      this.router.navigate(['/tabs/more/alquran/surah'], {
          queryParams: data,
      });


    }

    
}
