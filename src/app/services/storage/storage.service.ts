import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { NativeStorage } from '@ionic-native/native-storage/ngx';
import { Platform } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class StorageService {

  countries: any = [];

  constructor(
    public platform: Platform,
    private storage: Storage,
    private nativeStorage: NativeStorage,
  ) {


  }

  getItem(name) {

    if (this.platform.is('desktop')) {
      return this.storage.get(name);
    } else {
      return this.nativeStorage.getItem(name);
    }

  }

  setItem(name, object) {

    if (this.platform.is('desktop')) {
      return this.storage.set(name, object);
    } else {
      return this.nativeStorage.setItem(name, object);
    }

  }

}
