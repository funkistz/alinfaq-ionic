import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class WordpressService {

  // baseUrl = 'https://app5.al-infaq.com/wp-json/wp/v2/';
  baseUrl = 'https://backapp.al-infaq.com/wp-json/wp/v2/';

  constructor(private http: HttpClient) { }

  getCustom(endpoint) {
    return this.http.get(this.baseUrl + endpoint);
  }
}
