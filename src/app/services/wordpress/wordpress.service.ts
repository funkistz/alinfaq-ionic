import { Injectable } from '@angular/core';
import hmacSHA1 from 'crypto-js/hmac-sha1';
import Base64 from 'crypto-js/enc-base64';

@Injectable({
  providedIn: 'root'
})
export class WordPressService {
  nonce = '';
  currentTimestamp = 0;
  customer_key = 'ck_2e777dd6fdca2df7b19f26dcf58e2988c5ed1f6d';
  customer_secret = 'cs_0176cb5343903fbaebdd584c3c947ff034ecab90';
  constructor() { }

  authenticateApi(method, url, params) {
    const possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    this.nonce = '';
    for (let i = 0; i < 6; i++) {
      this.nonce += possible.charAt(Math.floor(Math.random() * possible.length));
    }
    this.currentTimestamp = Math.floor(new Date().getTime() / 1000);

    const authParam: object = {
      oauth_consumer_key: this.customer_key,
      oauth_nonce: this.nonce,
      oauth_signature_method: 'HMAC-SHA1',
      oauth_timestamp: this.currentTimestamp,
      oauth_version: '1.0',
    };
    const parameters = Object.assign({}, authParam, params);
    let signatureStr = '';
    Object.keys(parameters).sort().forEach(function (key) {
      if (signatureStr == '') {
        signatureStr += key + '=' + parameters[key];
      } else {
        signatureStr += '&' + key + '=' + parameters[key];
      }
    });
    let paramStr = '';
    Object.keys(params).sort().forEach(function (key) {

      paramStr += '&' + key + '=' + parameters[key];
    });
    return url + '?oauth_consumer_key=' + this.customer_key + '&oauth_nonce=' + this.nonce + '&oauth_signature_method=HMAC-SHA1&oauth_timestamp=' + this.currentTimestamp + '&oauth_version=1.0&oauth_signature=' + Base64.stringify(hmacSHA1(method + '&' + encodeURIComponent(url) + '&' + encodeURIComponent(signatureStr), this.customer_secret + '&')) + paramStr;

  }
}
