import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import 'firebase/firestore';
import { Alquran, Verse } from '../../shared/Alquran';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AlquranService {

  constructor(public firestore: AngularFirestore) { }

  createSurah(
    name: string,
    name_arabic: string,
    position: number,
  ): Promise<void> {
    const id = this.firestore.createId();

    return this.firestore.doc(`alquran/${id}`).set({
      id,
      name,
      name_arabic,
      position
    });
  }

  getSurahList(): Observable<Alquran[]> {
    return this.firestore.collection<Alquran>('alquran', ref => ref.orderBy('position')).valueChanges();
  }

  getSurahDetail(id: string): Observable<Alquran> {
    return this.firestore.collection<Alquran>('alquran', ref => ref.orderBy('position')).doc<Alquran>(id).valueChanges();
  }

  updateSurah(surah): Promise<void> {

    return this.firestore.collection<Alquran>('alquran').doc(surah.id).set(
      surah, { merge: true });
  }

  getVerseList(surah_id): Observable<Verse[]> {
    return this.firestore.collection<Alquran>('alquran').doc(surah_id).collection<Verse>('verses', ref => ref.orderBy('position')).valueChanges();
  }

  createVerse(
    surah_id: string,
    verse: string,
    verse_en: string,
    verse_my: string,
    position: number,
  ): Promise<void> {
    const id = this.firestore.createId();

    return this.firestore.doc(`alquran/${surah_id}`).collection('verses').doc(id).set({
      id,
      verse,
      verse_en,
      verse_my,
      position
    });
  }

  getVerseDetail(surah_id: string, id: string): Observable<Verse> {
    return this.firestore.collection<Alquran>('alquran').doc(surah_id).collection<Verse>('verses').doc<Verse>(id).valueChanges();
  }

  updateVerse(surah_id, verse): Promise<void> {

    return this.firestore.collection<Alquran>('alquran').doc(surah_id).collection<Verse>('verses').doc<Verse>(verse.id).set(
      verse, { merge: true });
  }

  deleteSurah(surahId: string): Promise<void> {
    return this.firestore.collection<Alquran>('alquran').doc(surahId).delete();
  }

  deleteVerse(surahId: string, verseId: string): Promise<void> {
    return this.firestore.collection<Alquran>('alquran').doc(surahId).collection<Verse>('verses').doc<Verse>(verseId).delete();
  }

  // // Create
  // createAlquran(apt: Alquran) {
  //   return this.alquranListRef.push({
  //     name: apt.name,
  //     name_arabic: apt.name_arabic,
  //     position: apt.position
  //   })
  // }

  // // Get Single
  // getAlquran(id: string) {
  //   this.alquranRef = this.db.object('/alquran/' + id);
  //   return this.alquranRef;
  // }

  // // Get List
  // getAlquranList() {
  //   this.alquranListRef = this.db.list('/alquran');
  //   return this.alquranListRef;
  // }

  // // Update
  // updateAlquran(id, apt: Alquran) {
  //   return this.alquranRef.update({
  //     name: apt.name,
  //     name_arabic: apt.name_arabic,
  //     position: apt.position
  //   })
  // }

  // // Delete
  // deleteAlquran(id: string) {
  //   this.alquranRef = this.db.object('/alquran/' + id);
  //   this.alquranRef.remove();
  // }
}
