import { TestBed } from '@angular/core/testing';

import { SolatService } from './solat.service';

describe('SolatService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SolatService = TestBed.get(SolatService);
    expect(service).toBeTruthy();
  });
});
