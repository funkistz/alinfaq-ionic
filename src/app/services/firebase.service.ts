import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument, DocumentReference } from '@angular/fire/firestore';
import { map, take } from 'rxjs/operators';
import { Observable } from 'rxjs';
import * as firebase from 'firebase/app';
import * as moment from 'moment';

export interface Solat {
  id?: string;
  date: string;
  hijri: string;
  day: string;

  dhuhr: Date;
  fajr: Date;
  imsak: Date;
  isha: Date;
  maghrib: Date;
  syuruk: Date;
}

@Injectable({
  providedIn: 'root'
})
export class FirebaseService {
  private solat: Observable<Solat[]>;
  private solatCollection: AngularFirestoreCollection<Solat>;

  constructor(private afs: AngularFirestore) {

    this.solatCollection = this.afs.collection('prayerTimeZone');
    this.solat = this.solatCollection.snapshotChanges().pipe(
      map((actions: any) => {
        return actions.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return { id, ...data };
        });
      })
    );

  }

  getAllPrayerTimes(zone) {

    return this.solatCollection.doc(zone).collection('prayerTime').snapshotChanges().pipe(
      map((actions: any) => {
        return actions.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return { id, ...data };
        });
      })
    );

  }

  addPrayerTime(solat, zone): Promise<void> {

    const key = moment().format('YYYYMMDD');

    return this.solatCollection.doc(zone).collection('prayerTime').doc(solat.date).set(solat, { merge: true });
  }

  getFirebasePrayer(prayerDate, zone): Observable<any> {

    const key = moment(prayerDate).format('YYYY-MM-DD');

    return this.solatCollection.doc(zone).collection('prayerTime').doc(key).valueChanges().pipe(
      take(1),
      map(prayer => {
        console.log('prayer prayer', prayer);

        if (prayer) {
          const temp: any = prayer;
          temp.id = key;
          return temp;
        } else {
          return null;
        }

      })
    );
  }

  getFirebasePrayerCollection(prayerDate, zone): any {
    return this.solatCollection.doc(zone).collection('prayerTime');
  }
}
