import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map, take } from 'rxjs/operators';
import { NativeStorage } from '@ionic-native/native-storage/ngx';
import { LocalNotifications } from '@ionic-native/local-notifications/ngx';
import * as moment from 'moment';
import { FirebaseService } from '../services/firebase.service';
import { StorageService } from '../services/storage/storage.service';
import { Settings } from '../data/settings';

@Injectable({
  providedIn: 'root'
})
export class SolatService {

  constructor(
    private http: HttpClient,
    private nativeStorage: NativeStorage,
    private localNotifications: LocalNotifications,
    private firebaseService: FirebaseService,
    private storageService: StorageService,
    public settings: Settings,
  ) { }

  prayerToday() {

    const url = 'http://api.azanpro.com/times/today.json?zone=WLY01';

    return this.http.get(url);
  }

  getAllPrayer(zone) {

    const url = 'https://www.e-solat.gov.my/index.php?r=esolatApi/takwimsolat&period=year&zone=' + zone;

    return this.http.get(url);

  }

  getNextPrayer(prayerTimes) {

    let timestamp = moment().format('HH:mm:ss');
    let currentDate = moment().format('DD-MMM-YYYY');

    if (prayerTimes[0].date.match(/[a-z]/i)) {
      currentDate = currentDate.replace('Mar', 'Mac');
      currentDate = currentDate.replace('May', 'Mei');
      currentDate = currentDate.replace('Aug', 'Ogos');
      currentDate = currentDate.replace('Oct', 'Okt');
      currentDate = currentDate.replace('Dec', 'Dis');
    } else {
      timestamp = moment().format('HH:mm:ss');
      currentDate = moment().format('YYYY-MM-DD');
    }

    const foundRef = prayerTimes.find(prayer => {
      return prayer.date == currentDate;
    });
    const found = JSON.parse(JSON.stringify(foundRef));

    if (!prayerTimes[0].date.match(/[a-z]/i)) {

      found.fajr = moment(found.fajr).format('HH:mm:ss');
      found.dhuhr = moment(found.dhuhr).format('HH:mm:ss');
      found.asr = moment(found.asr).format('HH:mm:ss');
      found.maghrib = moment(found.maghrib).format('HH:mm:ss');
      found.isha = moment(found.isha).format('HH:mm:ss');
    }

    if (timestamp <= found.fajr) {
      return {
        text: 'Subuh',
        timeStamp: found.fajr
      };
    } else if (timestamp <= found.dhuhr) {
      return {
        text: 'Zohor',
        timeStamp: found.dhuhr
      };
    } else if (timestamp <= found.asr) {
      return {
        text: 'Asar',
        timeStamp: found.asr
      };
    } else if (timestamp <= found.maghrib) {
      return {
        text: 'Maghrib',
        timeStamp: found.maghrib
      };
    } else if (timestamp <= found.isha) {
      return {
        text: 'Ishak',
        timeStamp: found.isha
      };
    } else {

      const tmr = moment(new Date()).add(1, 'days').format('DD-MMM-YYYY');
      const foundTmr = prayerTimes.find(prayer => {
        return prayer.date == tmr;
      });

      return {
        text: 'Subuh',
        timeStamp: foundTmr.fajr
      };

    }

  }

  setNotifications(prayerTimes) {

    console.log('set prayer times...');

    this.localNotifications.clearAll();
    this.localNotifications.cancelAll();

    if (prayerTimes && prayerTimes.prayerTime) {

      // number of notification set
      const limit = 12;
      let count = 0;
      const prayerQueue = [];
      let prayerWord = false;

      if (prayerTimes.prayerTime[0].date.match(/[a-z]/i)) {
        prayerWord = true;
      }

      for (const prayerTime of prayerTimes.prayerTime) {

        // console.log('prayerTime', JSON.stringify(prayerTime));

        let prayerDate = prayerTime.date;

        if (prayerWord) {
          // alphabet letters found
          prayerDate = prayerDate.replace('Mac', 'Mar');
          prayerDate = prayerDate.replace('Mei', 'May');
          prayerDate = prayerDate.replace('Ogos', 'Aug');
          prayerDate = prayerDate.replace('Okt', 'Oct');
          prayerDate = prayerDate.replace('Dis', 'Dec');

          prayerDate = moment(prayerDate, 'DD-MMM-YYYY');
        } else {
          prayerDate = moment(prayerDate, 'YYYY-MM-DD');
        }

        if (prayerDate.format('YYYYMMDD') >= moment().format('YYYYMMDD')) {

          if (count >= limit) {
            break;
          }

          let subuhTime = moment(prayerTime.fajr);
          let zohorTime = moment(prayerTime.dhuhr);
          let asarTime = moment(prayerTime.asr);
          let maghribTime = moment(prayerTime.maghrib);
          let ishaTime = moment(prayerTime.isha);

          if (prayerWord) {
            subuhTime = moment(prayerDate.format('YYYY-MM-DD' + ' ' + prayerTime.fajr));
            zohorTime = moment(prayerDate.format('YYYY-MM-DD' + ' ' + prayerTime.dhuhr));
            asarTime = moment(prayerDate.format('YYYY-MM-DD' + ' ' + prayerTime.asr));
            maghribTime = moment(prayerDate.format('YYYY-MM-DD' + ' ' + prayerTime.maghrib));
            ishaTime = moment(prayerDate.format('YYYY-MM-DD' + ' ' + prayerTime.isha));
          }

          // console.log('subuhTime', prayerTime.fajr);
          // console.log('zohorTime', prayerTime.dhuhr);
          // console.log('asarTime', prayerTime.asr);
          // console.log('maghribTime', prayerTime.maghrib);
          // console.log('ishaTime', prayerTime.isha);

          const prayerNotis = [
            {
              id: prayerDate.format('YYYYMMDD') + 1,
              title: 'Subuh Prayer at ' + subuhTime.clone().format('hh:mm A'),
              trigger: subuhTime,
            },
            {
              id: prayerDate.format('YYYYMMDD') + 2,
              title: 'Zohor Prayer at ' + zohorTime.clone().format('hh:mm A'),
              trigger: zohorTime,
            },
            {
              id: prayerDate.format('YYYYMMDD') + 3,
              title: 'Asar Prayer at ' + asarTime.clone().format('hh:mm A'),
              trigger: asarTime,
            },
            {
              id: prayerDate.format('YYYYMMDD') + 4,
              title: 'Maghrib Prayer at ' + maghribTime.clone().format('hh:mm A'),
              trigger: maghribTime,
            },
            {
              id: prayerDate.format('YYYYMMDD') + 5,
              title: 'Isha Prayer at ' + ishaTime.clone().format('hh:mm A'),
              trigger: ishaTime,
            }
          ];

          prayerNotis.forEach(prayerNoti => {

            if (prayerNoti.trigger.isValid()) {
              prayerQueue.push(
                {
                  id: Number(prayerNoti.id),
                  title: prayerNoti.title,
                  text: 'View prayer times',
                  trigger: { at: prayerNoti.trigger.toDate() },
                  foreground: true,
                  vibrate: true,
                  sound: 'file://assets/audio/azan.mp3',
                  // icon: 'res://icon.png',
                  // smallIcon: 'res://icon.png'
                });
            }

          });

          count++;
        }

      }

      // console.log('now', moment(new Date()).toDate());
      // console.log('prayerQueue', JSON.stringify(prayerQueue));

      console.log('prayer queue', 'prayer queue 12');
      this.localNotifications.schedule(prayerQueue);

    }

  }

  setFirebaseNotifications(zone) {

    this.localNotifications.clearAll();
    this.localNotifications.cancelAll();

    this.firebaseService.getAllPrayerTimes(zone).subscribe(prayerTimes => {

      if (prayerTimes && prayerTimes.length > 0) {

        // number of notification set
        const limit = 12;
        let count = 0;
        const prayerQueue = [];

        let currentDate = moment();

        for (const prayerTemp of prayerTimes) {
          const prayerTime: any = prayerTemp;

          let prayerDate = prayerTime.date;
          prayerDate = moment(prayerDate, 'YYYY-MM-DD');

          if (prayerDate.format('YYYYMMDD') >= moment().format('YYYYMMDD')) {

            if (count >= limit) {
              break;
            }

            // const subuhTime = moment(prayerTime.fajr.toDate());
            // const zohorTime = moment(prayerTime.dhuhr.toDate());
            // const asarTime = moment(prayerTime.asr.toDate());
            // const maghribTime = moment(prayerTime.maghrib.toDate());
            // const ishaTime = moment(prayerTime.isha.toDate());

            const subuhTime = currentDate.add(1, 'minutes');
            const zohorTime = currentDate.add(2, 'minutes');
            const asarTime = currentDate.add(3, 'minutes');
            const maghribTime = currentDate.add(4, 'minutes');
            const ishaTime = currentDate.add(5, 'minutes');

            currentDate = currentDate.add(6, 'minutes');

            const prayerNotis = [
              {
                id: prayerDate.format('YYYYMMDD') + 1,
                title: 'Subuh Prayer at ' + subuhTime.clone().format('hh:mm A'),
                trigger: subuhTime,
              },
              {
                id: prayerDate.format('YYYYMMDD') + 2,
                title: 'Zohor Prayer at ' + zohorTime.clone().format('hh:mm A'),
                trigger: zohorTime,
              },
              {
                id: prayerDate.format('YYYYMMDD') + 3,
                title: 'Asar Prayer at ' + asarTime.clone().format('hh:mm A'),
                trigger: asarTime,
              },
              {
                id: prayerDate.format('YYYYMMDD') + 4,
                title: 'Maghrib Prayer at ' + maghribTime.clone().format('hh:mm A'),
                trigger: maghribTime,
              },
              {
                id: prayerDate.format('YYYYMMDD') + 5,
                title: 'Isha Prayer at ' + ishaTime.clone().format('hh:mm A'),
                trigger: ishaTime,
              }
            ];

            prayerNotis.forEach(prayerNoti => {

              if (prayerNoti.trigger.isValid()) {
                prayerQueue.push(
                  {
                    id: Number(prayerNoti.id),
                    title: prayerNoti.title,
                    text: 'View prayer times',
                    trigger: { at: prayerNoti.trigger.toDate() },
                    foreground: true,
                    vibrate: true,
                    sound: 'file://assets/audio/azan.mp3',
                    // icon: 'res://icon.png',
                    // smallIcon: 'res://icon.png'
                  });
              }

            });

            count++;

          }

        }

        this.localNotifications.schedule(prayerQueue);
        console.log('setFirebaseNotifications: ', prayerQueue);

      }

    }, error => {
      console.log('getAllPrayerTimes error: ', error);
    });

  }

  addMinutes(date, minutes) {
    return new Date(date.getTime() + minutes * 60000);
  }

  getAllPrayerTimes(prayerTimes) {

    let prayerWord = false;
    if (prayerTimes.prayerTime[0].date.match(/[a-z]/i)) {
      prayerWord = true;
    }

    // tslint:disable-next-line: prefer-const
    // let prayerTimesModified = prayerTimes.prayerTime;

    prayerTimes.prayerTime.forEach(prayerTime => {

      let prayerDate = prayerTime.date;

      if (prayerWord) {
        prayerDate = prayerDate.replace('Mac', 'Mar');
        prayerDate = prayerDate.replace('Mei', 'May');
        prayerDate = prayerDate.replace('Ogos', 'Aug');
        prayerDate = prayerDate.replace('Okt', 'Oct');
        prayerDate = prayerDate.replace('Dis', 'Dec');
        prayerDate = moment(prayerDate, 'DD-MMM-YYYY');
        prayerTime.date = prayerDate.format('YYYY-MM-DD');
        prayerTime.asr = moment(prayerDate.format('YYYY-MM-DD' + ' ' + prayerTime.asr)).format();
        prayerTime.dhuhr = moment(prayerDate.format('YYYY-MM-DD' + ' ' + prayerTime.dhuhr)).format();
        prayerTime.fajr = moment(prayerDate.format('YYYY-MM-DD' + ' ' + prayerTime.fajr)).format();
        prayerTime.imsak = moment(prayerDate.format('YYYY-MM-DD' + ' ' + prayerTime.imsak)).format();
        prayerTime.isha = moment(prayerDate.format('YYYY-MM-DD' + ' ' + prayerTime.isha)).format();
        prayerTime.maghrib = moment(prayerDate.format('YYYY-MM-DD' + ' ' + prayerTime.maghrib)).format();
        prayerTime.syuruk = moment(prayerDate.format('YYYY-MM-DD' + ' ' + prayerTime.syuruk)).format();
      } else {
        prayerDate = moment(prayerDate);
        prayerTime.date = prayerDate.format('YYYY-MM-DD');
        prayerTime.asr = moment(prayerTime.asr).format();
        prayerTime.dhuhr = moment(prayerTime.dhuhr).format();
        prayerTime.fajr = moment(prayerTime.fajr).format();
        prayerTime.imsak = moment(prayerTime.imsak).format();
        prayerTime.isha = moment(prayerTime.isha).format();
        prayerTime.maghrib = moment(prayerTime.maghrib).format();
        prayerTime.syuruk = moment(prayerTime.syuruk).format();
      }

      // if (prayerTime.date === moment().format('YYYY-MM-DD')) {
      //   console.log('prayerTime awal', prayerTime.dhuhr);
      //   console.log('prayerTime awal', JSON.stringify(prayerTime));
      // }

      // if (prayerTime.date === moment().subtract(1, 'days').format('YYYY-MM-DD')) {
      //   console.log('prayerTime lambat', prayerTime.dhuhr);
      //   console.log('prayerTime lambat', JSON.stringify(prayerTime));
      // }

    });

    this.storageService.getItem('selected_zone').then(selectedZone => {

      if (selectedZone) {
        this.setFirebaseData(prayerTimes.prayerTime, selectedZone);
      }

    });

    return prayerTimes;

  }

  setFirebaseData(prayertimes, zone) {

    console.log('setFirebaseData', prayertimes);

    if (!zone) {
      return;
    }

    let index = 1;
    const limit = 2000;

    for (const prayerTime of prayertimes) {

      if (index > limit) {
        break;
      }

      prayerTime.asr = moment(prayerTime.asr).toDate();
      prayerTime.dhuhr = moment(prayerTime.dhuhr).toDate();
      prayerTime.fajr = moment(prayerTime.fajr).toDate();
      prayerTime.imsak = moment(prayerTime.imsak).toDate();
      prayerTime.isha = moment(prayerTime.isha).toDate();
      prayerTime.maghrib = moment(prayerTime.maghrib).toDate();
      prayerTime.syuruk = moment(prayerTime.syuruk).toDate();

      // console.log('firebase prayerTime', moment().utc(prayerTime).toDate());
      // console.log('firebase prayerTime', prayerTime);

      this.firebaseService.addPrayerTime(prayerTime, zone.code);

      index++;
    }

  }

  getFirebasePrayerToday(zone) {

    const date = new Date();

    this.firebaseService.getFirebasePrayer(date, zone.code).subscribe(prayerTime => {

      // console.log('getFirebasePrayerToday', prayerTime);
      // console.log('getFirebasePrayerToday date', prayerTime.fajr.toDate());

    });


  }

  getFirebaseNextPrayer(prayertoday, prayerTomorrow) {

    // console.log('prayertoday', prayertoday);
    // console.log('prayerTomorrow', prayerTomorrow);

    const timestamp = moment().format('HH:mm:ss');
    const currentDate = moment().format('DD-MMM-YYYY');

    // testing only
    // const timestamp = moment('2020-08-19T22:00:00').format('HH:mm:ss');

    prayertoday.fajr = moment(prayertoday.fajr.toDate()).format('HH:mm:ss');
    prayertoday.dhuhr = moment(prayertoday.dhuhr.toDate()).format('HH:mm:ss');
    prayertoday.asr = moment(prayertoday.asr.toDate()).format('HH:mm:ss');
    prayertoday.maghrib = moment(prayertoday.maghrib.toDate()).format('HH:mm:ss');
    prayertoday.isha = moment(prayertoday.isha.toDate()).format('HH:mm:ss');

    prayerTomorrow.fajr = moment(prayerTomorrow.fajr.toDate()).format('HH:mm:ss');
    prayerTomorrow.dhuhr = moment(prayerTomorrow.dhuhr.toDate()).format('HH:mm:ss');
    prayerTomorrow.asr = moment(prayerTomorrow.asr.toDate()).format('HH:mm:ss');
    prayerTomorrow.maghrib = moment(prayerTomorrow.maghrib.toDate()).format('HH:mm:ss');
    prayerTomorrow.isha = moment(prayerTomorrow.isha.toDate()).format('HH:mm:ss');


    if (timestamp <= prayertoday.fajr) {
      return {
        text: 'Subuh',
        timeStamp: prayertoday.fajr
      };
    } else if (timestamp <= prayertoday.dhuhr) {
      return {
        text: 'Zohor',
        timeStamp: prayertoday.dhuhr
      };
    } else if (timestamp <= prayertoday.asr) {
      return {
        text: 'Asar',
        timeStamp: prayertoday.asr
      };
    } else if (timestamp <= prayertoday.maghrib) {
      return {
        text: 'Maghrib',
        timeStamp: prayertoday.maghrib
      };
    } else if (timestamp <= prayertoday.isha) {
      return {
        text: 'Ishak',
        timeStamp: prayertoday.isha
      };
    } else {

      return {
        text: 'Subuh',
        timeStamp: prayerTomorrow.fajr
      };

    }

  }

  getFirebasePrayer(prayerDate, zone): Observable<any> {

    const key = moment(prayerDate).format('YYYY-MM-DD');

    return this.firebaseService.getFirebasePrayerCollection(prayerDate, zone).doc(key).valueChanges().pipe(
      take(1),
      map(prayer => {
        console.log('prayer prayer', prayer);

        if (prayer) {
          const temp: any = prayer;
          temp.id = key;
          return temp;
        } else {
          this.prayerApi(zone);
          return null;
        }

      })
    );
  }

  prayerApi(zone) {

    console.log('prayerApi', zone);

    this.getAllPrayer(zone).subscribe(result => {

      const data: any = result;

      if (result && data.prayerTime) {
        this.getAllPrayerTimes(result);

        const cacheKey = zone + '_' + new Date().getFullYear();

        this.nativeStorage.setItem(cacheKey, result).then(
          () => console.log('Stored item!'), error => console.error('Error storing prayer time API', error));
      }

    });
  }
}
