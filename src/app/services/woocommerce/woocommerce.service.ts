import { Injectable } from '@angular/core';
// import { Http, Headers, URLSearchParams } from '@angular/http';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class WoocommerceService {

  url = 'https://backapp.al-infaq.com/wc-mc.php';
  consumer_key = 'ck_1b7279613d5cde712f0e53a67fad586d442ff640';
  consumer_secret = 'cs_00787ba22acce5932ef7b509a722efaa97cc7f81';

  constructor(
    public http: HttpClient
  ) {

  }

  // method, wcmc, api, param
  get(option) {

    // console.log(option);

    const apiVersion = 'wc/v2';

    if (!option.param) {
      option.param = {};
    }

    if (option.method != 'GET') {
      option.params = this.objectToURLParams(option.param);
    }

    // const httpHeaders = new HttpHeaders();
    // const authorization = this.consumer_key + ':' + this.consumer_secret + ':' + version + ':' + option.api + ':' + option.method;
    // httpHeaders.set('x-api-key', authorization);

    const parameters = new HttpParams();
    parameters.set('consumer_key', this.consumer_key);
    parameters.set('consumer_secret', this.consumer_secret);
    parameters.set('version', apiVersion);
    parameters.set('endpoint', option.api);

    const data = [{
      parent: 22
    }];

    let params = {
      consumer_key: this.consumer_key,
      consumer_secret: this.consumer_secret,
      version: apiVersion,
      endpoint: option.api,
    };


    if (option.params) {
      params = { ...params, ...option.params };
    }

    if (option.method == 'GET') {

      let url = this.url;

      if (params) {
        url += '?' + this.objectToUrl(params);
      }

      console.log(url);
      return this.http.get(url);

    } else if (option.method == 'POST') {

      // return this.http.post(this.url, option.param, {
      //   headers: httpHeaders,
      // });

    } else if (option.method == 'PUT') {

      // return this.http.post(this.url, option.param, {
      //   headers: httpHeaders,
      // });

    } else if (option.method == 'DELETE') {

      // return this.http.post(this.url, option.param, {
      //   headers: httpHeaders,
      // });

    }

  }

  objectToUrl(data) {
    return Object.keys(data).map(key => `${key}=${encodeURIComponent(data[key])}`).join('&');
  }

  objectToURLParams(object): URLSearchParams {
    const params: URLSearchParams = new URLSearchParams();
    for (const key in object) {
      if (object.hasOwnProperty(key)) {
        if (Array.isArray(object[key])) {
          object[key].forEach(val => {
            params.append(key + '[]', val);
          });
        } else { params.set(key, object[key]); }
      }
    }
    return params;
  }
}
