import { Component } from '@angular/core';
import { LoadingController, NavController, ToastController, Platform } from '@ionic/angular';
import { DomSanitizer, SafeHtml, SafeStyle, SafeScript, SafeUrl, SafeResourceUrl } from '@angular/platform-browser';
import { ApiService } from '../api.service';
import { Settings } from '../data/settings';
import { Config } from '../config';

@Component({
    selector: 'app-tadhkirah',
    templateUrl: 'tadhkirah.page.html',
    styleUrls: ['tadhkirah.page.scss']
})
export class TadhkirahPage {

    videourl = 'https://www.youtube.com/embed/-tKVN2mAKRI';
    loading: any = true;
    posts: any = [];

    constructor(
        public sanitizer: DomSanitizer,
        public platform: Platform,
        public settings: Settings,
        public api: ApiService,
        private config: Config,
    ) {

        this.getPosts();
    }

    async getPosts() {

        const youtube_category = 152;
        const params = {
            _embed: true,
            categories: [youtube_category, this.config.lang_id],
            cat_relation: 'AND'
        };

        if (this.platform.is('hybrid')) {
            await this.api.getWPHybrid('posts', params).then((res) => {
                if (res) {
                    this.posts = res;
                    console.log('post', res);
                    this.posts.forEach(highlight => {

                        const youtubeLink = highlight.content.rendered.substring(
                            highlight.content.rendered.lastIndexOf('src="') + 5,
                            highlight.content.rendered.lastIndexOf('oembed"') + 6
                        );
                        highlight.youtubeLink = this.sanitizer.bypassSecurityTrustResourceUrl(youtubeLink);

                        const content = highlight.content.rendered.replace(
                            highlight.content.rendered.substring(
                                highlight.content.rendered.lastIndexOf('<figure'),
                                highlight.content.rendered.lastIndexOf('</figure>') + 9
                            )
                            , '');
                        highlight.contentClean = content;

                    });
                }

                this.loading = false;
                console.log(res);
            }, err => {
                this.loading = false;
                console.log(err);
            });
        } else {
            await this.api.getWP('posts', params).subscribe(res => {
                if (res) {
                    this.posts = res;
                    console.log('post', res);
                    this.posts.forEach(highlight => {
                        const youtubeLink = highlight.content.rendered.substring(
                            highlight.content.rendered.lastIndexOf('src="') + 5,
                            highlight.content.rendered.lastIndexOf('oembed"') + 6
                        );
                        highlight.youtubeLink = this.sanitizer.bypassSecurityTrustResourceUrl(youtubeLink);

                        const content = highlight.content.rendered.replace(
                            highlight.content.rendered.substring(
                                highlight.content.rendered.lastIndexOf('<figure'),
                                highlight.content.rendered.lastIndexOf('</figure>') + 9
                            )
                            , '');
                        highlight.contentClean = content;
                    });
                }

                this.loading = false;
                console.log(res);
            }, err => {
                this.loading = false;
                console.log(err);
            });
        }

    }

}
