import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SyncSolatPage } from './sync-solat.page';

describe('SyncSolatPage', () => {
  let component: SyncSolatPage;
  let fixture: ComponentFixture<SyncSolatPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SyncSolatPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SyncSolatPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
