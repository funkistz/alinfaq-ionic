import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SyncSolatPageRoutingModule } from './sync-solat-routing.module';

import { SyncSolatPage } from './sync-solat.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SyncSolatPageRoutingModule
  ],
  declarations: [SyncSolatPage]
})
export class SyncSolatPageModule {}
