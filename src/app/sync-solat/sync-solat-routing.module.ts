import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SyncSolatPage } from './sync-solat.page';

const routes: Routes = [
  {
    path: '',
    component: SyncSolatPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SyncSolatPageRoutingModule {}
