import { Component, OnInit } from '@angular/core';
import { Settings } from '../data/settings';
import { NavController, ToastController } from '@ionic/angular';
import { HttpClient } from '@angular/common/http';
import * as moment from 'moment';
import { FirebaseService } from '../services/firebase.service';

@Component({
  selector: 'app-sync-solat',
  templateUrl: './sync-solat.page.html',
  styleUrls: ['./sync-solat.page.scss'],
})
export class SyncSolatPage implements OnInit {

  log = '';
  loading = false;

  constructor(
    public settings: Settings,
    public toastController: ToastController,
    private http: HttpClient,
    private firebaseService: FirebaseService,
  ) { }

  ngOnInit() {
    console.log(this.settings);
    moment.locale('en');
  }

  sync() {

    this.loading = true;
    // console.log(this.settings.settings.zone_list);

    if (this.settings.settings.zone_list) {

      this.settings.settings.zone_list.forEach(zones => {

        zones.data.forEach(zonex => {
          this.prayerApi(zonex.code);
        });

      });

    } else {
      this.presentToast('No zone found');
    }
  }

  async presentToast(message) {
    const toast = await this.toastController.create({
      message,
      duration: 2000,
      position: 'top'
    });
    toast.present();
  }

  getAllPrayer(zone) {

    const url = 'https://www.e-solat.gov.my/index.php?r=esolatApi/takwimsolat&period=year&zone=' + zone;

    return this.http.get(url);


  }

  async prayerApi(zone) {

    console.log('prayerApi', zone);

    await this.getAllPrayer(zone).subscribe(result => {

      // console.log('getAllPrayer', result);

      const data: any = result;
      let index = 0;

      if (result && data.prayerTime && index == 0) {
        this.getAllPrayerTimes(result, zone);

        index++;

        // const cacheKey = zone + '_' + new Date().getFullYear();
      }

    });
  }

  async getAllPrayerTimes(prayerTimes, zone) {

    let prayerWord = false;
    if (prayerTimes.prayerTime[0].date.match(/[a-z]/i)) {
      prayerWord = true;
    }

    // tslint:disable-next-line: prefer-const
    // let prayerTimesModified = prayerTimes.prayerTime;

    // for (let prayerTime of prayerTimes.prayerTime) {



    // }

    await prayerTimes.prayerTime.forEach(prayerTime => {

      let prayerDate = prayerTime.date;

      if (prayerWord) {
        prayerDate = prayerDate.replace('Mac', 'Mar');
        prayerDate = prayerDate.replace('Mei', 'May');
        prayerDate = prayerDate.replace('Ogos', 'Aug');
        prayerDate = prayerDate.replace('Okt', 'Oct');
        prayerDate = prayerDate.replace('Dis', 'Dec');
        prayerDate = moment(prayerDate);
        prayerTime.date = prayerDate.format('YYYY-MM-DD');
        prayerTime.asr = moment(prayerDate.format('YYYY-MM-DD' + ' ' + prayerTime.asr)).format();
        prayerTime.dhuhr = moment(prayerDate.format('YYYY-MM-DD' + ' ' + prayerTime.dhuhr)).format();
        prayerTime.fajr = moment(prayerDate.format('YYYY-MM-DD' + ' ' + prayerTime.fajr)).format();
        prayerTime.imsak = moment(prayerDate.format('YYYY-MM-DD' + ' ' + prayerTime.imsak)).format();
        prayerTime.isha = moment(prayerDate.format('YYYY-MM-DD' + ' ' + prayerTime.isha)).format();
        prayerTime.maghrib = moment(prayerDate.format('YYYY-MM-DD' + ' ' + prayerTime.maghrib)).format();
        prayerTime.syuruk = moment(prayerDate.format('YYYY-MM-DD' + ' ' + prayerTime.syuruk)).format();
      } else {
        prayerDate = moment(prayerDate);
        prayerTime.date = prayerDate.format('YYYY-MM-DD');
        prayerTime.asr = moment(prayerTime.asr).format();
        prayerTime.dhuhr = moment(prayerTime.dhuhr).format();
        prayerTime.fajr = moment(prayerTime.fajr).format();
        prayerTime.imsak = moment(prayerTime.imsak).format();
        prayerTime.isha = moment(prayerTime.isha).format();
        prayerTime.maghrib = moment(prayerTime.maghrib).format();
        prayerTime.syuruk = moment(prayerTime.syuruk).format();
      }

      // console.log('prayerTimes xxx', prayerTimes);

      // if (prayerTime.date === moment().format('YYYY-MM-DD')) {
      //   console.log('prayerTime awal', prayerTime.dhuhr);
      //   console.log('prayerTime awal', JSON.stringify(prayerTime));
      // }

      // if (prayerTime.date === moment().subtract(1, 'days').format('YYYY-MM-DD')) {
      //   console.log('prayerTime lambat', prayerTime.dhuhr);
      //   console.log('prayerTime lambat', JSON.stringify(prayerTime));
      // }

    });

    this.setFirebaseData(prayerTimes.prayerTime, zone);

    return prayerTimes;

  }

  setFirebaseData(prayertimes, zone) {

    console.log('setFirebaseData', prayertimes);

    if (!zone) {
      return;
    }

    let index = 1;
    const limit = 2000;

    for (const prayerTime of prayertimes) {

      if (index > limit) {
        break;
      }

      prayerTime.asr = moment(prayerTime.asr).toDate();
      prayerTime.dhuhr = moment(prayerTime.dhuhr).toDate();
      prayerTime.fajr = moment(prayerTime.fajr).toDate();
      prayerTime.imsak = moment(prayerTime.imsak).toDate();
      prayerTime.isha = moment(prayerTime.isha).toDate();
      prayerTime.maghrib = moment(prayerTime.maghrib).toDate();
      prayerTime.syuruk = moment(prayerTime.syuruk).toDate();

      // console.log('firebase prayerTime', moment().utc(prayerTime).toDate());
      // console.log('firebase prayerTime', prayerTime);
      // console.log('zone.code', zone);

      this.firebaseService.addPrayerTime(prayerTime, zone).then(
        () => {
          this.log += '<br>Success for ' + zone
        }, error => {
          this.log += '<br>Error for ' + zone
        });

      index++;
    }

  }

}
