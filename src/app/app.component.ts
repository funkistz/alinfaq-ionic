import { Component } from '@angular/core';
import { Platform, AlertController, NavController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { AppMinimize } from '@ionic-native/app-minimize/ngx';
import { TranslateService } from '@ngx-translate/core';
import { NativeStorage } from '@ionic-native/native-storage/ngx';
import { Config } from './config';
import { StorageService } from './services/storage/storage.service';
import { ApiService } from './api.service';
import { Data } from './data';
import { Settings } from './data/settings';
import { OneSignal } from '@ionic-native/onesignal/ngx';
import { BackgroundMode } from '@ionic-native/background-mode/ngx';
import { ScreenOrientation } from '@ionic-native/screen-orientation/ngx';

declare var wkWebView: any;

@Component({
    selector: 'app-root',
    templateUrl: 'app.component.html'
})

export class AppComponent {

    cart: any;
    filter: any = {};
    popup: any;

    constructor(
        private config: Config,
        private nativeStorage: NativeStorage,
        public translateService: TranslateService,
        public platform: Platform,
        private splashScreen: SplashScreen,
        private statusBar: StatusBar,
        private appMinimize: AppMinimize,
        private storageService: StorageService,
        public api: ApiService,
        public data: Data,
        public settings: Settings,
        public alertController: AlertController,
        private oneSignal: OneSignal,
        public navCtrl: NavController,
        private backgroundMode: BackgroundMode,
        private screenOrientation: ScreenOrientation
    ) {
        this.initializeApp();
    }

    initializeApp() {
        this.platform.ready().then(() => {

            if (this.platform.is('android')) {
                this.statusBar.overlaysWebView(false);
                this.statusBar.backgroundColorByHexString('#000000');
                this.statusBar.styleLightContent();
            }

            setTimeout(() => {
                this.splashScreen.hide();
            }, 500);

            this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);

            document.addEventListener('deviceready', () => {
                wkWebView.injectCookie(this.config.url + '/');
            });

            // this.statusBar.backgroundColorByHexString('#ffffff');

            /* Add your translation file in src/assets/i18n/ and set your default language here */
            this.storageService.getItem('language').then(language => {
                if (language) {
                    this.translateService.setDefaultLang(language);
                    this.settings.lan = language;
                } else {
                    this.translateService.setDefaultLang('en');
                    this.storageService.setItem('language', 'en');
                    this.settings.lan = 'en';
                }

            }, error => {
                this.translateService.setDefaultLang('en');
                this.storageService.setItem('language', 'en');
                this.settings.lan = 'en';
            });

            this.storageService.getItem('language_id').then(language_id => {
                if (language_id) {
                    this.storageService.setItem('language_id', language_id);
                    this.config.lang_id = language_id;
                } else {
                    this.storageService.setItem('language_id', 43);
                }

            }, error => {
                this.storageService.setItem('language_id', 43);
            });
            // document.documentElement.setAttribute('dir', 'rtl');


            this.getBlocks();

            // this.statusBar.backgroundColorByHexString('#004a91');
            // this.statusBar.backgroundColorByHexString('#ffffff');
            // this.statusBar.styleBlackTranslucent();
            // this.statusBar.styleLightContent();

            // this.minimize();
            this.platform.backButton.subscribeWithPriority(0, () => {
                this.appMinimize.minimize();
            });

            this.backgroundMode.on('activate').subscribe(() => {
                // this.backgroundMode.disableWebViewOptimizations();
            });
            this.backgroundMode.on('deactivate').subscribe(() => {
            });
            this.backgroundMode.disableBatteryOptimizations();

            this.backgroundMode.enable();

        });
    }

    getBlocks() {

        if (!this.platform.is('desktop')) {
            return;
        }

        this.api.postItem('keys').subscribe(res => {

            this.data.blocks = res;

            console.log('blocks app.component', res);

            if (this.data.blocks.popup.image) {
                this.presentPopup(this.data.blocks.popup.image.image);
            }

            if (this.data.blocks.user) {
                this.settings.user = this.data.blocks.user.data;
            }
            // this.settings.theme = this.data.blocks.theme;
            this.settings.pages = this.data.blocks.pages;
            if (this.data.blocks.user) {
                this.settings.reward = this.data.blocks.user.data.points_vlaue;
            }
            if (this.data.blocks.languages) {
                this.settings.languages = Object.keys(this.data.blocks.languages).map(i => this.data.blocks.languages[i]);
            }
            this.settings.currencies = this.data.blocks.currencies;
            this.settings.settings = this.data.blocks.settings;

            // check app version
            if (this.settings.settings.app_version <= this.data.appVersion) {
                console.log('version ok');
            } else {
                console.log('version not ok');
                this.presentAlertUpdate();
            }

            this.settings.dimensions = this.data.blocks.dimensions;
            this.settings.currency = this.data.blocks.settings.currency;

            if (this.data.blocks.categories) {
                this.data.categories = this.data.blocks.categories.filter(item => item.name != 'Uncategorized');
                this.data.mainCategories = this.data.categories.filter(item => item.parent == 0);
            }
            this.settings.calc(this.platform.width());
            if (this.settings.colWidthLatest == 4) { this.filter.per_page = 15; }
            this.getCart();
            this.processOnsignal();
            if (this.data.blocks.user) {
                this.settings.customer = this.data.blocks.user.data;
                this.settings.customer.id = this.data.blocks.user.ID;
                // tslint:disable-next-line: max-line-length
                if (this.data.blocks.user.allcaps.dc_vendor || this.data.blocks.user.allcaps.seller || this.data.blocks.user.allcaps.wcfm_vendor) {
                    this.settings.vendor = true;
                }
            }
            for (const item in this.data.blocks.blocks) {
                if (this.data.blocks.blocks[item].block_type == 'flash_sale_block') {
                    this.data.blocks.blocks[item].interval = setInterval(() => {
                        const countDownDate = new Date(this.data.blocks.blocks[item].sale_ends).getTime();
                        const now = new Date().getTime();
                        const distance = countDownDate - now;
                        this.data.blocks.blocks[item].days = Math.floor(distance / (1000 * 60 * 60 * 24));
                        this.data.blocks.blocks[item].hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
                        this.data.blocks.blocks[item].minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
                        this.data.blocks.blocks[item].seconds = Math.floor((distance % (1000 * 60)) / 1000);
                        if (distance < 0) {
                            clearInterval(this.data.blocks.blocks[item].interval);
                            this.data.blocks.blocks[item].hide = true;
                        }
                    }, 1000);
                }
            }
            if (this.data.blocks.settings.show_latest) {
                this.data.products = this.data.blocks.recentProducts;
            }
            if (this.data.blocks.user) {
                this.api.postItem('get_wishlist').subscribe(res => {
                    for (const item in res) {
                        this.settings.wishlist[res[item].id] = res[item].id;
                    }
                }, err => {
                    console.log(err);
                });
            }

            this.nativeStorage.setItem('blocks', {
                blocks: this.data.blocks,
                categories: this.data.categories
            }).then(
                () => console.log('Stored item!'), error => console.error('Error storing item', error));

            /* Product Addons */
            if (this.data.blocks.settings.switchAddons) {
                this.api.getAddonsList('product-add-ons').subscribe(res => {
                    this.settings.addons = res;
                });
            }

        }, err => {
            console.log(err);
        });
    }

    async presentAlertUpdate() {
        const alert = await this.alertController.create({
            header: 'Application is not up to date!',
            message: 'Please update the app',
            backdropDismiss: false,
            // buttons: [
            //     {
            //         text: 'Okay',
            //         handler: () => {
            //             console.log('Confirm Okay');
            //         }
            //     }
            // ]
        });

        await alert.present();
    }

    processOnsignal() {
        this.oneSignal.startInit(this.data.blocks.settings.onesignal_app_id, this.data.blocks.settings.google_project_id);
        // this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.InAppAlert);
        this.oneSignal.handleNotificationReceived().subscribe(() => {
            // do something when notification is received
        });
        this.oneSignal.handleNotificationOpened().subscribe(result => {
            if (result.notification.payload.additionalData.category) {
                this.navCtrl.navigateForward('/tabs/home/products/' + result.notification.payload.additionalData.category);
            } else if (result.notification.payload.additionalData.product) {
                this.navCtrl.navigateForward('/tabs/home/product/' + result.notification.payload.additionalData.product);
            } else if (result.notification.payload.additionalData.post) {
                this.navCtrl.navigateForward('/tabs/home/post/' + result.notification.payload.additionalData.post);
            } else if (result.notification.payload.additionalData.order) {
                this.navCtrl.navigateForward('/tabs/account/orders/order/' + result.notification.payload.additionalData.order);
            }
        });
        this.oneSignal.endInit();
    }

    getCart() {
        this.api.postItem('cart').subscribe(res => {
            this.cart = res;
            this.data.updateCart(this.cart.cart_contents);
            this.data.cartNonce = this.cart.cart_nonce;
        }, err => {
            console.log(err);
        });
    }

    async presentPopup(imageSrc) {
        const popup = await this.alertController.create({
            mode: 'md',
            cssClass: 'alert-popup',
            buttons: [
                {
                    text: 'X',
                    role: 'cancel',
                    cssClass: 'btn-close',
                    handler: () => {
                        console.log('Confirm Cancel');
                    }
                }
            ],
            message: '<div><img src="' +
                imageSrc +
                '" class="card-alert"></div>'
        });

        // <ion-button (click)="dismissPopup()" color="light" size="small" class="btn-close"><ion-icon slot="icon-only" name="close"></ion-icon></ion-button>

        await popup.present();
    }

    dismissPopup() {
        console.log('dismissing...');
        if (this.popup) {
            this.popup.dismiss();
        }
    }
}
