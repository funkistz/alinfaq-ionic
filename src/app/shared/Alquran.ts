export class Alquran {
    $key: string;
    id: string;
    name: string;
    name_arabic: string;
    position: number;
}

export class Verse {
    $key: string;
    id: string;
    verse: string;
    verse_en: string;
    verse_my: string;
    position: number;
}