import { Component, OnInit, Input } from '@angular/core';
import * as moment from 'moment';
import * as momenthijri from 'moment-hijri';
import * as momenttimezone from 'moment-timezone';
import { Settings } from '../../data/settings';
import { NativeStorage } from '@ionic-native/native-storage/ngx';
import { AlertController, NavController } from '@ionic/angular';
import * as HijrahDate from 'hijrah-date';
import { HttpClient } from '@angular/common/http';
import { StorageService } from '../../services/storage/storage.service';
import { HTTP } from '@ionic-native/http/ngx';

@Component({
  selector: 'app-datebar',
  templateUrl: './datebar.component.html',
  styleUrls: ['./datebar.component.scss'],
})
export class DatebarComponent implements OnInit {

  @Input() prayerTime;

  today = '';
  location = '';
  date = '';
  dateHijri = '';

  allPrayer: any = [];
  currentPrayerTime = {
    text: '',
    timeStamp: ''
  };
  prayetTime;
  prayetAMPM;

  constructor(
    public settings: Settings,
    private nativeStorage: NativeStorage,
    public alertController: AlertController,
    public navCtrl: NavController,
    private http: HttpClient,
    private nativehttp: HTTP,
    private storageService: StorageService,
  ) {
    const momentOffset = moment().utcOffset();
    momenthijri().utcOffset(4);
    moment.locale('en');

    const testDate = momenthijri.utc(new Date()).utcOffset(4).format('iDD iMMMM iYYYY');
    console.log('testDate', testDate);

    this.today = moment().format('dddd');
    this.location = 'putrajaya';
    this.date = moment().format('DD MMMM YYYY');
    // this.dateHijri = momenthijri(new Date()).format('iDD iMMMM iYYYY');

    console.log('this.dateHijri', this.dateHijri);
    console.log('momentOffset', momenthijri().utcOffset());

    this.setDate();
    setInterval(() => {
      this.setDate();
    }, 10000);
  }

  setDate() {
    // console.log('set date...');

    this.date = moment().format('DD MMMM YYYY');
    const today = moment().format('YYYYMMDD');

    this.storageService.getItem('todayDate').then(todayDate => {

      if (today === todayDate && this.dateHijri) {
        // console.log('same day');
      } else {
        this.getHijrahDate(today);
        this.storageService.setItem('todayDate', today);
        // console.log('not same day');
      }

    }, err => {
      this.getHijrahDate(today);
      this.storageService.setItem('todayDate', today);
      // console.log('no day');
    });


  }

  getHijrahDate(today) {

    // const url = 'https://hijrah.mfrapps.com/api/hijrah-api.php?tarikh=' + today;
    const url = 'https://backapp.al-infaq.com/hijrahDate.php?date=' + today;

    // this.nativehttp.get(url, {}, {})
    //   .then(xmlStr => {

    //     console.log('success hijrah', xmlStr);

    //     const jsonP = this.parseXml(xmlStr);

    //     console.log('success hijrah parse', jsonP);

    //     this.dateHijri = jsonP;

    //   })
    //   .catch(error => {

    //     console.log('error hijrah parse', error);
    //     console.log(error.status);
    //     console.log(error.error);
    //     console.log(error.headers);

    //   });

    this.http.get(url, { responseType: 'text' }).subscribe(xmlStr => {

      // console.log('hijrah', xmlStr);
      // const jsonP = this.parseXml(xmlStr);
      this.dateHijri = xmlStr;
    });
  }

  parseXml(xmlStr) {
    let result;
    const parser = require('xml2js');
    parser.Parser().parseString(xmlStr, (e, r) => { result = r; });

    if (result.date && result.date.hijrah) {

      const dateTemp = result.date.hijrah[0];
      const dateH = dateTemp.day[0] + ' ' + dateTemp.month[0] + ' ' + dateTemp.year[0];

      return dateH;

    } else {
      return '';
    }
  }

  checkLocation() {

    this.nativeStorage.getItem('zone').then(zone => {
      if (zone) {

      }
    }, error => console.error(error));

  }

  ngOnInit() { }

  selectZone() {
    this.navCtrl.navigateForward('/tabs/home/zone');
  }

}
