import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DatebarComponent } from './datebar.component';

describe('DatebarComponent', () => {
  let component: DatebarComponent;
  let fixture: ComponentFixture<DatebarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DatebarComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DatebarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
