import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { Settings } from '../../data/settings';
import { Data } from '../../data';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-menubar',
  templateUrl: './menubar.component.html',
  styleUrls: ['./menubar.component.scss'],
})
export class MenubarComponent implements OnInit {

  @Input() activeMenu;

  mainButtons = [
    {
      name: '. . .',
      image_path: '/assets/icon/infaq.png',
    },
    {
      name: '. . .',
      image_path: '/assets/icon/wakaf.png',
    },
    {
      name: '. . .',
      image_path: '/assets/icon/zakat.png',
    },
    {
      name: '. . .',
      image_path: '/assets/icon/tajaan.png',
    }
  ];

  infaqTypes: any;

  constructor(
    public router: Router,
    public settings: Settings,
    public data: Data,
    public translateService: TranslateService,
  ) { }

  redirectInfaq(infaq) {

    // console.log('infaq type', infaq);

    this.router.navigate(['/tabs/home/infaq'], {
      queryParams: {
        infaqType: infaq.id
      },
    });
  }

  ngOnInit() {

    // this.infaqTypes = this.data.infaqTypes;
    // console.log('activeMenu', this.activeMenu);
  }

}
