import { Component, OnInit, Input, ViewChild, ElementRef } from '@angular/core';
import { Router, NavigationExtras } from '@angular/router';
import { LoadingController, NavController, AlertController, ToastController, Platform } from '@ionic/angular';

@Component({
  selector: 'app-horizontal-card-list',
  templateUrl: './horizontal-card-list.component.html',
  styleUrls: ['./horizontal-card-list.component.scss'],
})
export class HorizontalCardListComponent implements OnInit {

  @Input() title;
  @Input() contents;
  @Input() allPage;
  @Input() type;
  @Input() delayed = 0;
  @Input() active = true;

  // @ViewChild('widgetsContent', { static: false }) widgetsContent: ElementRef;
  @ViewChild('widgetsContent', { static: false }) public widgetsContent: ElementRef<any>;

  limit = 500;
  duration = 10000;
  currentscroll = 0;
  jump = 1;
  delay = 10;
  onTouch = false;
  direction = 'right';

  constructor(
    public router: Router,
    public navCtrl: NavController,
    public platform: Platform,

  ) { }

  ngOnInit() {

    this.platform.ready().then(() => {
      const check = setInterval(() => {

        if (this.contents.length > 0) {

          clearInterval(check);
          setTimeout(() => {
            this.scrollRight();
          }, this.delayed);

        }

      }, 1000);
    });

  }

  ionViewDidEnter() {
    this.touchend();
  }

  ionViewDidLeave() {
    this.touchstart();
  }

  myLoopRight() {
    // setTimeout(() => {

    //   if (!this.onTouch) {

    //     this.widgetsContent.nativeElement.scrollTo({ left: (this.widgetsContent.nativeElement.scrollLeft + this.jump), behavior: 'smooth' });
    //     this.currentscroll = this.widgetsContent.nativeElement.scrollLeft + this.jump;

    //     if (this.currentscroll < this.limit) {
    //       this.myLoopRight();
    //     } else {

    //       setTimeout(() => {
    //         this.myLoopLeft();
    //       }, 500);

    //     }
    //   } else {
    //     this.myLoopRight();
    //   }

    // }, this.delay);

    let count = 1;
    const rightLoop = setInterval(() => {

      if (!this.onTouch) {

        const diff = this.limit - this.widgetsContent.nativeElement.scrollLeft;
        const percent = Math.min(count / this.duration, 1);
        // console.log('diff', diff);
        // console.log('duration', this.duration);
        // console.log('percent', percent);

        // this.widgetsContent.nativeElement.scrollTo({ left: (this.widgetsContent.nativeElement.scrollLeft + this.jump), behavior: 'smooth' });

        this.currentscroll = this.widgetsContent.nativeElement.scrollLeft + Math.min(diff * percent, 1) + 10;
        this.widgetsContent.nativeElement.scrollTo({
          left: (this.widgetsContent.nativeElement.scrollLeft + Math.min(diff * percent, 1)),
          behavior: 'smooth'
        });

        count++;

        if (this.currentscroll >= this.limit) {

          clearInterval(rightLoop);
          this.myLoopLeft();
        }

      }

    }, this.delay);
  }

  myLoopLeft() {
    // setTimeout(() => {

    //   if (!this.onTouch) {

    //     this.widgetsContent.nativeElement.scrollTo({ left: (this.widgetsContent.nativeElement.scrollLeft - this.jump)});
    //     this.currentscroll = this.widgetsContent.nativeElement.scrollLeft - this.jump;

    //     if (this.currentscroll > 0) {
    //       this.myLoopLeft();
    //     } else {

    //       setTimeout(() => {
    //         this.myLoopRight();
    //       }, 500);

    //     }

    //   } else {
    //     this.myLoopLeft();
    //   }

    // }, this.delay);

    let count = 1;
    const leftLoop = setInterval(() => {

      if (!this.onTouch) {

        const diff = this.widgetsContent.nativeElement.scrollLeft - 0;
        const percent = Math.min(count / this.duration, 1);

        // this.widgetsContent.nativeElement.scrollTo({ left: (this.widgetsContent.nativeElement.scrollLeft - this.jump), behavior: 'smooth' });

        this.currentscroll = this.widgetsContent.nativeElement.scrollLeft - Math.min(diff * percent, 1);
        this.widgetsContent.nativeElement.scrollTo({
          left: (this.widgetsContent.nativeElement.scrollLeft - Math.min(diff * percent, 1)),
          behavior: 'smooth'
        });

        count++;

        if (this.currentscroll <= 0) {

          clearInterval(leftLoop);
          this.myLoopRight();
        }

      }

    }, this.delay);
  }

  public scrollRight(): void {

    this.limit = this.widgetsContent.nativeElement.scrollWidth - this.widgetsContent.nativeElement.offsetWidth;
    // this.duration = (this.widgetsContent.nativeElement.scrollWidth - this.widgetsContent.nativeElement.scrollLeft) * 10 * 2;
    this.duration = (this.widgetsContent.nativeElement.scrollWidth) * 10 * 2;

    this.currentscroll = 0;
    // this.widgetsContent.nativeElement.scrollTo({ left: (0) });

    // this.myLoopRight();

    setTimeout(() => {

      if (this.direction === 'right') {
        this.doScrolling(this.limit, this.duration);
      } else {
        this.doScrollingB(0, this.duration);
      }
    }, 200);

  }

  public scrollLeft(): void {
    this.widgetsContent.nativeElement.scrollTo({ left: (this.widgetsContent.nativeElement.scrollLeft - 150), behavior: 'smooth' });
  }

  touchstart() {
    this.onTouch = true;
  }

  touchend() {

    this.onTouch = false;
    this.scrollRight();
  }

  navigate(news) {

    const data = {
      title: this.title
    };

    this.router.navigate(['/tabs/home/news/' + news.id], {
      queryParams: data,
    });
  }

  viewAll(categoryId) {

    if (!categoryId) {
      return;
    }

    const data = {
      title: this.title,
      postCategory: categoryId
    };

    this.router.navigate(['/tabs/news'], {
      queryParams: data,
    });
  }

  private doScrolling(limit, duration) {
    this.direction = 'right';
    this.duration = (this.limit - this.widgetsContent.nativeElement.scrollLeft) * 10 * 3;

    const startingY = this.widgetsContent.nativeElement.scrollLeft;
    const diff = limit - startingY;
    let start;
    const widgetsContent = this.widgetsContent.nativeElement;
    const context = this;

    window.requestAnimationFrame(function step(timestamp) {

      if (context.onTouch) {
        return;
      }

      start = (!start) ? timestamp : start;
      const time = timestamp - start;
      const percent = Math.min(time / context.duration, 1);

      // this.widgetsContent.nativeElement.scrollTo(0, startingY + diff * percent);
      widgetsContent.scrollTo({ left: (startingY + diff * percent) });

      if (context.widgetsContent.nativeElement.scrollLeft < context.limit) {
        window.requestAnimationFrame(step);
      } else {
        context.doScrollingB(0, context.duration);
      }
    });
  }

  private doScrollingB(limit, duration) {

    this.direction = 'left';
    this.duration = (this.widgetsContent.nativeElement.scrollLeft) * 10 * 3;

    const startingY = this.widgetsContent.nativeElement.scrollLeft;
    const diff = startingY - limit;
    let start;
    const widgetsContent = this.widgetsContent.nativeElement;
    const context = this;

    window.requestAnimationFrame(function stepB(timestamp) {

      if (context.onTouch) {
        return;
      }

      start = (!start) ? timestamp : start;
      const time = timestamp - start;
      const percent = Math.min(time / context.duration, 1);

      widgetsContent.scrollTo({ left: (startingY - diff * percent) });

      if (context.widgetsContent.nativeElement.scrollLeft > 0) {
        window.requestAnimationFrame(stepB);
      } else {
        context.doScrolling(context.limit, context.duration);
      }
    });
  }

}
