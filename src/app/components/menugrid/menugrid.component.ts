import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-menugrid',
  templateUrl: './menugrid.component.html',
  styleUrls: ['./menugrid.component.scss'],
})
export class MenugridComponent implements OnInit {

  menus = [
    {
      title: 'News',
      icon: '/assets/icon/subicon-news.png',
      link: '/news'
    },
    {
      title: 'Gallery',
      icon: '/assets/icon/subicon-gallery.png',
      link: ''
    },
    {
      title: 'Receiver',
      icon: '/assets/icon/subicon-applicant.png',
      link: ''
    },
    {
      title: 'History',
      icon: '/assets/icon/subicon-orderhistory.png',
      link: ''
    }
  ];

  constructor(public router: Router) { }

  ngOnInit() { }

  navigate(link) {
    console.log('navigate', link);
    this.router.navigate([link], {});
  }

}
