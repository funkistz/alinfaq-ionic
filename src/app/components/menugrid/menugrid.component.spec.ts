import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { MenugridComponent } from './menugrid.component';

describe('MenugridComponent', () => {
  let component: MenugridComponent;
  let fixture: ComponentFixture<MenugridComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MenugridComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(MenugridComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
