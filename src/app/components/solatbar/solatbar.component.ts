import { Component, OnInit } from '@angular/core';
import { SolatService } from './../../services/solat.service';
import * as moment from 'moment';
import { ApiService } from '../../api.service';

@Component({
  selector: 'app-solatbar',
  templateUrl: './solatbar.component.html',
  styleUrls: ['./solatbar.component.scss'],
})
export class SolatbarComponent implements OnInit {

  allPrayer: any = [];
  currentPrayerTime = {
    text: '',
    timeStamp: ''
  };
  prayetTime;
  prayetAMPM;

  constructor(
    private solatService: SolatService,
    public api: ApiService
  ) {

  }

  ngOnInit() {

    this.getAllPrayer('WLY01');

    // this.getPrayerPost();

    // this.solatService.prayerToday().subscribe(result => {

    //   const data: any = result;

    //   this.currentPrayerTime = this.solatService.getNextPrayer(data.prayer_times, moment().unix());

    //   console.log('currentPrayerTime', data);
    //   this.prayetTime = moment.unix(Number(this.currentPrayerTime.timeStamp)).format('h:mm');
    //   this.prayetAMPM = moment.unix(Number(this.currentPrayerTime.timeStamp)).format('A');

    // });

  }

  async getPrayerPost() {

    await this.api.getWP('posts/112', {

    }).subscribe(res => {
      console.log(res);
    }, err => {
      console.log(err);
    });

  }

  getAllPrayer(zone) {

    this.solatService.getAllPrayer(zone).subscribe(result => {

      const data: any = result;

      // console.log(moment().format('DD-MMM-YYYY'), data);

      let currentDate = moment().format('DD-MMM-YYYY');
      currentDate = currentDate.replace('Mar', 'Mac');

      const found = data.prayerTime.find(prayer => {
        return prayer.date == currentDate;
      });

      // console.log('date', currentDate);
      // this.currentPrayerTime = this.solatService.getNextPrayer(found, moment().format('HH:mm:ss'));

      // const found = data[0].filter(x => x.date == moment().format('DD-MMM-YYYY'))[0];

      // console.log(this.currentPrayerTime);
      // let currentTime = moment(this.currentPrayerTime.timeStamp, 'HH:mm:ss');
      this.prayetTime = moment(this.currentPrayerTime.timeStamp, 'HH:mm:ss').format('h:mm');
      this.prayetAMPM = moment(this.currentPrayerTime.timeStamp, 'HH:mm:ss').format('A');

    });

    // this.nativeStorage.getItem('prayer_times_' + zone)
    //   .then(
    //     data => {
    //       console.log(data);
    //     },
    //     error => console.error(error)
    //   );

    // this.nativeStorage.setItem('prayer_times_' + zone, { property: 'value', anotherProperty: 'anotherValue' })
    //   .then(
    //     (data) => console.log('Stored first item!', data),
    //     error => console.error('Error storing item', error)
    //   );

  }

}
