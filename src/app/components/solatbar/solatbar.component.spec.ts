import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SolatbarComponent } from './solatbar.component';

describe('SolatbarComponent', () => {
  let component: SolatbarComponent;
  let fixture: ComponentFixture<SolatbarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SolatbarComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SolatbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
