import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';

import { MenubarComponent } from './menubar/menubar.component';
import { DatebarComponent } from '../components/datebar/datebar.component';
import { MenugridComponent } from '../components/menugrid/menugrid.component';
import { HorizontalCardListComponent } from '../components/horizontal-card-list/horizontal-card-list.component';
import { SolatbarComponent } from '../components/solatbar/solatbar.component';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
    imports: [
        CommonModule,
        IonicModule,
        RouterModule,
        TranslateModule
    ],
    declarations: [
        MenubarComponent,
        DatebarComponent,
        MenugridComponent,
        HorizontalCardListComponent,
        SolatbarComponent
    ],
    exports: [
        MenubarComponent,
        DatebarComponent,
        MenugridComponent,
        HorizontalCardListComponent,
        SolatbarComponent
    ]
})

export class ComponentsModule { }
