import { Component, OnInit, ViewChild } from '@angular/core';
import { NavController, Platform } from '@ionic/angular';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from '../../api.service';
import { CheckoutData } from '../../data/checkout';
import { Settings } from './../../data/settings';

@Component({
    selector: 'app-address',
    templateUrl: './address.page.html',
    styleUrls: ['./address.page.scss'],
})
export class CheckoutAddressPage implements OnInit {
    errorMessage: any;
    loader = false;
    loader2 = false;
    countries: any;
    receivers: any = [];
    parentReceiver = {
        id: 0,
        children: []
    };
    choosenReceiver = {
        id: 0,
        name: ''
    };
    hasReceiver = true;

    constructor(
        public api: ApiService,
        public checkoutData: CheckoutData,
        public router: Router,
        public navCtrl: NavController,
        public settings: Settings,
        public route: ActivatedRoute,
        public platform: Platform,
    ) { }

    ngOnInit() {
        this.getCheckoutForm();
        this.getReceivers();

        this.route.queryParams.subscribe((res) => {
            this.hasReceiver = (res.receiver === 'true');

            if (!this.hasReceiver) {
                this.choosenReceiver.id = 999;
            }
            console.log('receiver', this.hasReceiver);
        });
    }

    setupAddress() {

        this.checkoutData.form.shipping_first_name = '-';
        this.checkoutData.form.shipping_last_name = '-';
        this.checkoutData.form.shipping_company = '';
        this.checkoutData.form.shipping_address_1 = '';
        this.checkoutData.form.shipping_address_2 = '';
        this.checkoutData.form.shipping_city = '-';
        this.setupAddressAfterCheckout();

        if (!this.hasReceiver) {
            this.checkoutData.form.shipping_address_1 = 'not set';
            this.checkoutData.form.shipping_address_2 = 'not set';
        }

    }

    setupAddressAfterCheckout() {
        this.checkoutData.form.shipping_postcode = this.checkoutData.form.billing_postcode;
        this.checkoutData.form.shipping_country = this.checkoutData.form.billing_country;
        this.checkoutData.form.shipping_state = this.checkoutData.form.billing_state;
    }

    async getCheckoutForm() {
        this.loader = true;
        await this.api.postItem('get_checkout_form').subscribe(res => {

            console.log('checkout form', res);

            this.checkoutData.form = res;
            this.checkoutData.form.sameForShipping = true;
            if (this.checkoutData.form.countries.length == 1) {
                this.checkoutData.form.billing_country = this.checkoutData.form.countries[0].value;
                // this.checkoutData.form.shipping_country = this.checkoutData.form.countries[0].value;
            }
            this.checkoutData.billingStates = this.checkoutData.form.countries.find(
                item => item.value == this.checkoutData.form.billing_country
            );
            this.checkoutData.shippingStates = this.checkoutData.form.countries.find(
                item => item.value == this.checkoutData.form.shipping_country
            );

            this.checkoutData.form.ship_to_different_address = true;
            this.loader = false;

            this.setupAddress();

        }, err => {
            console.log(err);
            this.loader = false;
        });
    }

    async getReceivers() {

        const params = {
            parent: 45,
        };

        if (this.platform.is('hybrid')) {
            await this.api.getItemIonic('products/categories', params).then((res) => {
                if (res) {
                    console.log('receiver', JSON.stringify(res));
                    this.processReceiver(res);
                }
            }, err => {
                console.log(err);
            });
        } else {
            // receivers id = 45
            await this.api.getItem('products/categories', params).subscribe(res => {
                if (res) {
                    console.log('receiver', JSON.stringify(res));
                    this.processReceiver(res);
                }
            }, err => {
                console.log(err);
            });
        }

    }

    processReceiver(res) {

        this.receivers = res;
        this.receivers.forEach(parentreceivers => {

            this.loader2 = true;

            if (parentreceivers.slug === 'individual') {
                parentreceivers.icon = 'person-outline';
            } else if (parentreceivers.slug === 'organization') {
                parentreceivers.icon = 'people-outline';
            }

            if (this.platform.is('hybrid')) {

                this.api.getItemIonic('products/categories', {
                    parent: parentreceivers.id,
                }).then(res2 => {
                    this.loader2 = false;

                    if (res2) {
                        parentreceivers.children = res2;
                    }
                }, err => {
                    console.log(err);
                });

            } else {

                this.api.getItem('products/categories', {
                    parent: parentreceivers.id,
                }).subscribe(res2 => {
                    this.loader2 = false;

                    if (res2) {
                        parentreceivers.children = res2;
                    }
                }, err => {
                    console.log(err);
                });

            }

        });
        console.log('processReceiver', JSON.stringify(this.receivers));
    }

    chooseParentReceiver(receiver) {

        this.choosenReceiver = {
            id: 0,
            name: ''
        };
        this.parentReceiver = receiver;
        this.checkoutData.form.shipping_address_1 = receiver.name;

    }

    chooseReceiver(receiver) {

        this.choosenReceiver = receiver;
        this.checkoutData.form.shipping_address_2 = receiver.name;
    }

    getCountries() {
        this.api.getItem('settings/general/woocommerce_specific_allowed_countries').subscribe(res => {
            this.countries = res;
        }, err => {
            console.log(err);
        });
    }

    getBillingRegion() {
        this.checkoutData.billingStates = this.checkoutData.form.countries.find(item => item.value == this.checkoutData.form.billing_country);
        this.checkoutData.form.billing_state = '';
    }

    getShippingRegion() {
        this.checkoutData.shippingStates = this.checkoutData.form.countries.find(item => item.value == this.checkoutData.form.shipping_country);
        this.checkoutData.form.shipping_state = '';
    }

    async updateOrderReview() {
        await this.api.postItem('update_order_review').subscribe(res => {
            this.checkoutData.orderReview = res;
        }, err => {
            console.log(err);
        });
    }

    continueCheckout() {

        this.errorMessage = '';
        this.setupAddressAfterCheckout();

        if (this.validateForm()) {
            if (!this.checkoutData.form.ship_to_different_address) {
                this.assgnShippingAddress();
            }

            this.navCtrl.navigateForward('/tabs/cart/checkout');
        }
    }

    validateForm() {
        if (this.checkoutData.form.billing_first_name == '' || this.checkoutData.form.billing_first_name == undefined) {
            this.errorMessage = 'Billing first name is a required field';
            return false;
        }

        if (this.checkoutData.form.billing_last_name == '' || this.checkoutData.form.billing_last_name == undefined) {
            this.errorMessage = 'Billing last name is a required field';
            return false;
        }

        if (this.checkoutData.form.billing_phone == '' || this.checkoutData.form.billing_phone == undefined) {
            this.errorMessage = 'Billing phone is a required field';
            return false;
        }

        if (this.checkoutData.form.billing_address_1 == '' || this.checkoutData.form.billing_address_1 == undefined) {
            this.errorMessage = 'Billing Street address is a required field';
            return false;
        }

        if (this.checkoutData.form.billing_city == '' || this.checkoutData.form.billing_city == undefined) {
            this.errorMessage = 'Billing city is a required field';
            return false;
        }

        if (this.checkoutData.form.billing_postcode == '' || this.checkoutData.form.billing_postcode == undefined) {
            this.errorMessage = 'Billing post code is a required field';
            return false;
        }

        if (this.checkoutData.form.billing_country == '' || this.checkoutData.form.billing_country == undefined) {
            this.errorMessage = 'Billing country is a required field';
            return false;
        }

        if (this.checkoutData.form.billing_state == '' || this.checkoutData.form.billing_state == undefined) {
            this.errorMessage = 'Billing state is a required field';
            return false;
        }

        if (this.checkoutData.form.ship_to_different_address) {
            if (this.checkoutData.form.shipping_first_name == '' || this.checkoutData.form.shipping_first_name == undefined) {
                this.errorMessage = 'Shipping first name is a required field';
                return false;
            }

            if (this.checkoutData.form.shipping_last_name == '' || this.checkoutData.form.shipping_last_name == undefined) {
                this.errorMessage = 'Shipping last name is a required field';
                return false;
            }

            if (this.checkoutData.form.shipping_address_1 == '' || this.checkoutData.form.shipping_address_1 == undefined) {
                this.errorMessage = 'Please choose a receiver';
                return false;
            }

            if (this.checkoutData.form.shipping_address_2 == '' || this.checkoutData.form.shipping_address_2 == undefined) {
                this.errorMessage = 'Please choose a receiver';
                return false;
            }

            if (this.checkoutData.form.shipping_city == '' || this.checkoutData.form.shipping_city == undefined) {
                this.errorMessage = 'Shipping city is a required field';
                return false;
            }

            if (this.checkoutData.form.shipping_postcode == '' || this.checkoutData.form.shipping_postcode == undefined) {
                this.errorMessage = 'Shipping post code is a required field';
                return false;
            }

            if (this.checkoutData.form.shipping_country == '' || this.checkoutData.form.shipping_country == undefined) {
                this.errorMessage = 'Shipping country is a required field';
                return false;
            }

            if (this.checkoutData.form.shipping_state == '' || this.checkoutData.form.shipping_state == undefined) {
                this.errorMessage = 'Shipping state is a required field';
                return false;
            }
            return true;
        } else { return true; }
    }

    assgnShippingAddress() {
        this.checkoutData.form.shipping_first_name = this.checkoutData.form.billing_first_name;
        this.checkoutData.form.shipping_last_name = this.checkoutData.form.billing_last_name;
        this.checkoutData.form.shipping_company = this.checkoutData.form.billing_company;
        this.checkoutData.form.shipping_address_1 = this.checkoutData.form.billing_address_1;
        this.checkoutData.form.shipping_address_2 = this.checkoutData.form.billing_address_2;
        this.checkoutData.form.shipping_city = this.checkoutData.form.billing_city;
        this.checkoutData.form.shipping_postcode = this.checkoutData.form.billing_postcode;
        this.checkoutData.form.shipping_country = this.checkoutData.form.billing_country;
        this.checkoutData.form.shipping_state = this.checkoutData.form.billing_state;
        return true;
    }
}
