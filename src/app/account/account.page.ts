import { Component } from '@angular/core';
import { NavController, Platform, AlertController, ActionSheetController } from '@ionic/angular';
import { Settings } from './../data/settings';
import { ApiService } from './../api.service';
import { AppRate } from '@ionic-native/app-rate/ngx';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { EmailComposer } from '@ionic-native/email-composer/ngx';
import { Config } from './../config';
import { TranslateService } from '@ngx-translate/core';
import { StorageService } from '../services/storage/storage.service';
import { LocalNotifications } from '@ionic-native/local-notifications/ngx';
import * as moment from 'moment';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
    selector: 'app-account',
    templateUrl: 'account.page.html',
    styleUrls: ['account.page.scss']
})
export class AccountPage {
    toggle: any;
    constructor(
        private config: Config,
        public api: ApiService,
        public navCtrl: NavController,
        public settings: Settings,
        public platform: Platform,
        private appRate: AppRate,
        private emailComposer: EmailComposer,
        private socialSharing: SocialSharing,
        public translateService: TranslateService,
        private alertController: AlertController,
        public actionSheetController: ActionSheetController,
        private storageService: StorageService,
        private localNotifications: LocalNotifications,
        public router: Router,
    ) {

        // this.localNotifications.get(2021).then(noti => {
        //     console.log('noti', noti);
        // });

        // this.localNotifications.get(2022).then(noti => {
        //     console.log('noti', noti);
        // });

        // console.log('localNotifications', this.localNotifications.getScheduledIds());

    }

    ionViewDidEnter() {

        console.log('getAllScheduled', JSON.stringify(this.localNotifications.getAllScheduled()));

        this.localNotifications.getScheduledIds().then(ids => {
            console.log('localNotifications ids', ids);
        });

        this.localNotifications.get(202008024).then(maghrib => {
            console.log('maghrib', maghrib);
        });

        // Schedule a single notification
        // this.localNotifications.schedule({
        //     id: Number(moment().format('YYYYMMDD')) + 99,
        //     title: 'Current Prayer at ' + moment().format('hh:mm A'),
        //     text: 'View prayer times',
        //     foreground: true,
        //     vibrate: true,
        //     // sound: 'res://azan.mp3',
        //     sound: 'file://assets/audio/azan.mp3'
        //     // icon: 'res://icon.png',
        //     // smallIcon: 'res://icon.png'
        // });

        // this.localNotifications.schedule({
        //     id: Number(moment().unix()),
        //     title: 'Delayed Prayer at ' + moment().format('hh:mm A'),
        //     text: 'View prayer times',
        //     trigger: { at: new Date(new Date().getTime() + 1 * 20000) },
        //     foreground: true,
        //     vibrate: true,
        //     // sound: 'res://azan.mp3',
        //     sound: 'file://assets/audio/azan.mp3'
        //     // icon: 'res://icon.png',
        //     // smallIcon: 'res://icon.png'
        // });

        // this.localNotifications.schedule({
        //     id: Number(moment().unix()) + 98,
        //     title: 'testing delayed 2',
        //     text: 'View prayer times',
        //     trigger: { at: new Date(new Date().getTime() + 1 * 120000) },
        //     foreground: true,
        //     vibrate: true,
        //     // sound: 'res://azan.mp3',
        //     // sound: 'file://assets/audio/azan.mp3'
        //     // icon: 'res://icon.png',
        //     // smallIcon: 'res://icon.png'
        // });

    }

    goTo(path) {
        this.navCtrl.navigateForward(path);
    }

    async log_out() {

        const alert = await this.alertController.create({
            header: 'Are you sure to logout?',
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                    cssClass: 'secondary',
                    handler: () => {
                    }
                }, {
                    text: 'Logout',
                    cssClass: 'danger',
                    handler: () => {

                        this.settings.customer.id = undefined;
                        this.settings.vendor = false;
                        this.settings.wishlist = [];
                        this.api.postItem('logout').subscribe(res => { }, err => {
                            console.log(err);
                        });
                        if ((window as any).AccountKitPlugin) {
                            (window as any).AccountKitPlugin.logout();
                        }

                    }
                }
            ]
        });

        await alert.present();

    }

    rateApp() {
        if (this.platform.is('cordova')) {
            this.appRate.preferences.storeAppURL = {
                ios: this.settings.settings.rate_app_ios_id,
                android: this.settings.settings.rate_app_android_id,
                windows: 'ms-windows-store://review/?ProductId=' + this.settings.settings.rate_app_windows_id
            };
            this.appRate.promptForRating(false);
        }
    }

    shareApp() {
        if (this.platform.is('cordova')) {
            let url = '';
            if (this.platform.is('android')) { url = this.settings.settings.share_app_android_link; } else { url = this.settings.settings.share_app_ios_link; }
            const options = {
                message: '',
                subject: '',
                files: ['', ''],
                url,
                chooserTitle: ''
            };
            this.socialSharing.shareWithOptions(options);
        }
    }

    email(contact) {
        const email = {
            to: contact,
            attachments: [],
            subject: '',
            body: '',
            isHtml: true
        };
        this.emailComposer.open(email);
    }

    ngOnInit() {

        console.log('setting', this.settings);

        this.toggle = document.querySelector('#themeToggle');
        // this.toggle.addEventListener('ionChange', (ev) => {
        //   document.body.classList.toggle('dark', ev.detail.checked);
        // });
        const prefersDark = window.matchMedia('(prefers-color-scheme: dark)');
        prefersDark.addListener((e) => checkToggle(e.matches));
        function loadApp() {
            checkToggle(prefersDark.matches);
        }
        function checkToggle(shouldCheck) {
            this.toggle.checked = shouldCheck;
        }
    }

    async change_language() {

        console.log('change language');

        let title = 'Language';
        this.translateService.get(title).subscribe(value => {
            title = value;
        });

        const actionSheet = await this.actionSheetController.create({
            header: title,
            cssClass: 'my-custom-class',
            buttons: [{
                text: 'English',
                handler: () => {
                    this.setLanguage('en', 43);
                }
            }, {
                text: 'Bahasa',
                handler: () => {
                    this.setLanguage('my', 42);
                }
            }, {
                text: 'Arabic',
                handler: () => {
                    this.setLanguage('ar', 44);
                }
            }, {
                text: 'Cancel',
                icon: 'close',
                role: 'cancel',
                handler: () => {
                    console.log('Cancel clicked');
                }
            }]
        });
        await actionSheet.present();

    }

    setLanguage(language, id) {

        this.translateService.use(language);
        this.storageService.setItem('language', language);
        this.storageService.setItem('language_id', id);
        this.config.lang_id = id;
        this.settings.lan = language;

        const params = {
            refresh: true
        };

        this.navCtrl.navigateRoot(['/tabs/home'], {
            queryParams: params,
        });
    }

}
