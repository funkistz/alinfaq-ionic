import { Component, OnInit } from '@angular/core';
import { LoadingController, NavController, Platform } from '@ionic/angular';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from '../../api.service';
import { Settings } from './../../data/settings';
import { NavigationExtras } from '@angular/router';

@Component({
    selector: 'app-orders',
    templateUrl: './orders.page.html',
    styleUrls: ['./orders.page.scss'],
})
export class OrdersPage implements OnInit {
    filter: any = {};
    orders: any;
    hasMoreItems = true;
    constructor(
        public api: ApiService,
        public settings: Settings,
        public router: Router,
        public loadingController: LoadingController,
        public navCtrl: NavController,
        public route: ActivatedRoute,
        public platform: Platform,
    ) {
        this.filter.page = 1;
        this.filter.customer = this.settings.customer.id;
    }

    ngOnInit() {
        if (this.settings.customer) {
            this.getOrders();
        }
    }

    async getOrders() {
        await this.api.postFlutterItem('orders', this.filter).subscribe(res => {
            this.orders = res;

            this.orders.forEach(order => {
                if (order.status === 'completed') {
                    order.postChecked = true;
                    this.getOrderPost(order);
                }
            });

        }, err => {
            console.log(err);
        });
    }

    checkOrdersPost(orders) {

    }

    async getOrderPost(order) {

        const ordername = 'order-' + order.id;
        const params = {
            per_page: 1,
            categories: 68,
            search: ordername,
        };

        if (this.platform.is('hybrid')) {
            await this.api.getWPHybrid('posts', params).then((res) => {
                // console.log('order res', res);

                if (res && res[0]) {
                    console.log('found');
                    order.deliverStatus = 'Your Infaq has been delivered, view now';
                    console.log(order);

                } else {
                    console.log('not found');
                }

            }, err => {
                console.log(err);
            });
        } else {
            await this.api.getWP('posts', params).subscribe(res => {
                // console.log('order', order);
                // console.log('order res', res);

                if (res && res[0]) {
                    console.log('found');
                    order.deliverStatus = 'Your Infaq has been delivered, view now';
                    console.log(order);
                } else {
                    console.log('not found');
                }

            }, err => {
                console.log(err);
            });
        }

    }

    async loadData(event) {
        this.filter.page = this.filter.page + 1;
        await this.api.postFlutterItem('orders', this.filter).subscribe(res => {
            this.orders.push.apply(this.orders, res);

            this.orders.forEach(order => {
                if (order.status === 'completed' && !order.postChecked) {
                    order.postChecked = true;
                    this.getOrderPost(order);
                }
            });

            event.target.complete();
            if (!res) { this.hasMoreItems = false; }
        }, err => {
            event.target.complete();
        });
        console.log('Done');
    }
    getDetail(order) {
        const navigationExtras: NavigationExtras = {
            queryParams: {
                order
            }
        };
        this.navCtrl.navigateForward('/tabs/account/orders/order/' + order.id, navigationExtras);
    }
}
