import { Component, OnInit } from '@angular/core';
import { LoadingController, NavController, Platform } from '@ionic/angular';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from '../../api.service';
import { Settings } from './../../data/settings';
import { ToastController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';

@Component({
    selector: 'app-order',
    templateUrl: './order.page.html',
    styleUrls: ['./order.page.scss'],
})
export class OrderPage implements OnInit {
    id: any;
    order: any;
    refundKeys: any = {};
    refund: any = {};
    showRefund = false;
    disableRefundButton = false;
    refundResponse: any = {};
    lan: any = {};
    orderPost: any;

    constructor(
        public translate: TranslateService,
        public api: ApiService,
        public settings: Settings,
        public toastController: ToastController,
        public router: Router,
        public loadingController: LoadingController,
        public navCtrl: NavController,
        public route: ActivatedRoute,
        public platform: Platform,
    ) { }

    async refundKey() {
        await this.api.postItem('woo_refund_key').subscribe(res => {
            this.refundKeys = res;
            console.log(this.refundKeys);
        }, err => {
            console.log(err);
        });
    }

    ngOnInit() {
        this.translate.get(['Refund request submitted!', 'Unable to submit the refund request']).subscribe(translations => {
            this.lan.refund = translations['Refund request submitted!'];
            this.lan.unable = translations['Unable to submit the refund request'];
        });
        this.id = this.route.snapshot.paramMap.get('id');
        this.route.queryParams.subscribe(params => {
            this.order = params.order;

            console.log(params.order);
        });

        if (this.order && this.order.status === 'completed') {
            this.getOrderPost(this.id);
        }

        // testing only
        this.api.getItem('orders/' + this.id).subscribe(res => {

            console.log('order', res);

        }, err => {
            console.log(err);
        });

        this.refundKey();
    }

    async getOrderPost(orderId) {

        const ordername = 'order-' + orderId;
        const params = {
            _embed: true,
            per_page: 1,
            categories: 68,
            search: ordername,
        };

        if (this.platform.is('hybrid')) {
            await this.api.getWPHybrid('posts', params).then((res) => {
                if (res && res[0]) {
                    this.processPost(res);
                }
            }, err => {
                console.log(err);
            });
        } else {
            await this.api.getWP('posts', params).subscribe(res => {
                if (res && res[0]) {
                    this.processPost(res);
                }
            }, err => {
                console.log(err);
            });
        }

    }

    processPost(res) {
        this.orderPost = res[0];
        if (this.orderPost._embedded && this.orderPost._embedded['wp:featuredmedia']) {
            this.orderPost.media = this.orderPost._embedded['wp:featuredmedia'][0].media_details.sizes.medium_large.source_url;
        } else {
            this.orderPost.media = '/assets/no-image.jpg';
        }
    }

    navigate(orderPost) {

        const data = {
            title: 'Infaq receiver',
            noTitle: true,
        };

        this.router.navigate(['/tabs/account/orders/order/' + this.id + '/post/' + orderPost.id], {
            queryParams: data,
        });
    }

    showField() {
        this.showRefund = !this.showRefund;
    }

    async requestRefund() {
        this.disableRefundButton = true;
        this.refund.ywcars_form_order_id = this.id;
        this.refund.ywcars_form_whole_order = '1';
        this.refund.ywcars_form_product_id = '';

        this.refund.ywcars_form_line_total = this.order.total;
        this.refund.ywcars_form_reason = this.refund.ywcars_form_reason;
        this.refund.action = 'ywcars_submit_request';
        this.refund.security = this.refundKeys.ywcars_submit_request;

        await this.api.postItem('woo_refund_key', this.refund).subscribe(res => {
            this.refundResponse = res;
            this.disableRefundButton = false;
            if (this.refundResponse.success) {
                this.presentToast(this.lan.refund);
            } else {
                this.presentToast(this.lan.unable);
            }
        }, err => {
            console.log(err);
            this.disableRefundButton = false;
        });
    }

    async presentToast(message) {
        const toast = await this.toastController.create({
            message,
            duration: 2000
        });
        toast.present();
    }
}
