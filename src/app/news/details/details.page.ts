import { Component, OnInit } from '@angular/core';
import { NavController, Platform } from '@ionic/angular';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from '../../api.service';
import { Settings } from '../../data/settings';

@Component({
    selector: 'app-details',
    templateUrl: 'details.page.html',
    styleUrls: ['details.page.scss']
})
export class DetailsPage {

    id: any;
    title: any = '';
    catId: any;
    post: any;
    country: any;
    noTitle = false;
    productId: any;

    constructor(
        public api: ApiService,
        public router: Router,
        public navCtrl: NavController,
        public settings: Settings,
        public route: ActivatedRoute,
        public platform: Platform,
    ) {

    }

    async getPost(id) {

        const params = {
            _embed: true,
        };

        if (this.platform.is('hybrid')) {
            await this.api.getWPHybrid('posts/' + id, params).then((res) => {
                if (res) {
                    this.post = res;
                    if (this.post.tags[0]) {
                        this.api.getWPHybrid('tags/' + this.post.tags[0], {
                        }).then(res2 => {
                            if (res) {
                                const tag: any = res2;
                                this.productId = tag.name;
                            }
                        }, err => {
                            console.log(err);
                        });
                    }
                }
            }, err => {
                console.log(err);
            });
        } else {
            await this.api.getWP('posts/' + id, params).subscribe(res => {
                if (res) {
                    this.post = res;
                    if (this.post.tags[0]) {
                        this.api.getWP('tags/' + this.post.tags[0], {
                        }).subscribe(res2 => {
                            if (res) {
                                const tag: any = res2;
                                this.productId = tag.name;
                            }
                        }, err => {
                            console.log(err);
                        });
                    }
                }
                console.log(res);
            }, err => {
                console.log(err);
            });
        }

    }
    ionViewWillEnter() {

        this.route.queryParams.subscribe((res) => {
            this.title = res.title;
            this.noTitle = res.noTitle;
        });
        this.id = this.route.snapshot.paramMap.get('id');
        this.getPost(this.id);

    }

    infaqNow(productId) {

        const params = {
            country: this.country,
            refresh: true
        };

        console.log('redirect', this.router.url);
        this.router.navigate(['/tabs/home/news/' + this.id + '/product/' + productId], {
            queryParams: params,
        });
    }
}
