import { Component, OnInit } from '@angular/core';
import { NavController, Platform } from '@ionic/angular';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from '../api.service';
import { Settings } from '../data/settings';
import { Config } from '../config';

@Component({
    selector: 'app-news',
    templateUrl: 'news.page.html',
    styleUrls: ['news.page.scss']
})
export class NewsPage {

    title: any = '';
    catId: any;
    posts: any = [];
    loading: any = true;
    origin: any = '';

    constructor(
        public api: ApiService,
        public router: Router,
        public navCtrl: NavController,
        public settings: Settings,
        public route: ActivatedRoute,
        public platform: Platform,
        private config: Config,
    ) {

    }

    async getPosts(catId) {

        const params = {
            _embed: true,
            categories: [catId, this.config.lang_id],
            cat_relation: 'AND'
        };

        if (this.platform.is('hybrid')) {
            await this.api.getWPHybrid('posts', params).then((res) => {
                if (res) {
                    this.posts = res;
                    this.posts.forEach(highlight => {
                        if (highlight._embedded && highlight._embedded['wp:featuredmedia']) {
                            highlight.media = highlight._embedded['wp:featuredmedia'][0].media_details.sizes.medium_large.source_url;
                        } else {
                            highlight.media = '/assets/no-image.jpg';
                        }
                    });
                    this.loading = false;
                    console.log(res);
                }
            }, err => {
                this.loading = false;
                console.log(err);
            });
        } else {
            await this.api.getWP('posts', params).subscribe(res => {
                this.posts = res;
                this.posts.forEach(highlight => {
                    if (res) {
                        if (highlight._embedded && highlight._embedded['wp:featuredmedia']) {
                            highlight.media = highlight._embedded['wp:featuredmedia'][0].media_details.sizes.medium_large.source_url;
                        } else {
                            highlight.media = '/assets/no-image.jpg';
                        }
                    }
                });
                this.loading = false;
                console.log(res);
            }, err => {
                this.loading = false;
                console.log(err);
            });
        }

    }

    ionViewWillEnter() {
        this.posts = [];
    }

    ngOnInit() {

        this.route.queryParams.subscribe((res) => {

            console.log(res);
            this.title = res.title;
            this.catId = res.postCategory;
            this.origin = res.origin;

            this.getPosts(this.catId);
        });
    }

    navigate(news) {

        const data = {
            title: this.title
        };

        this.router.navigate(['/tabs' + this.origin + '/news/' + news.id], {
            queryParams: data,
        });
    }

}
