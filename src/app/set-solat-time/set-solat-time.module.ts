import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SetSolatTimePageRoutingModule } from './set-solat-time-routing.module';

import { SetSolatTimePage } from './set-solat-time.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SetSolatTimePageRoutingModule
  ],
  declarations: [SetSolatTimePage]
})
export class SetSolatTimePageModule {}
