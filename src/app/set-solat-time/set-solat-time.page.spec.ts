import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SetSolatTimePage } from './set-solat-time.page';

describe('SetSolatTimePage', () => {
  let component: SetSolatTimePage;
  let fixture: ComponentFixture<SetSolatTimePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SetSolatTimePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SetSolatTimePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
