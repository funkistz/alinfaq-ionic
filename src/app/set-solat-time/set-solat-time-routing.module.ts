import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SetSolatTimePage } from './set-solat-time.page';

const routes: Routes = [
  {
    path: '',
    component: SetSolatTimePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SetSolatTimePageRoutingModule {}
