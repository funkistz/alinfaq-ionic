import { Component, OnInit, ViewChild } from '@angular/core';
import { NavController, ToastController, LoadingController, Platform, IonSelect } from '@ionic/angular';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from '../api.service';
import { Settings } from '../data/settings';
import { NativeStorage } from '@ionic-native/native-storage/ngx';
import { NavigationExtras } from '@angular/router';
import { NgStyleBase } from '@angular/common';
import { Data } from '../data';

@Component({
    selector: 'app-infaq',
    templateUrl: 'infaq.page.html',
    styleUrls: ['infaq.page.scss']
})
export class InfaqPage {

    constructor(
        public api: ApiService,
        public router: Router,
        public navCtrl: NavController,
        public settings: Settings,
        public route: ActivatedRoute,
        public toastController: ToastController,
        public loadingController: LoadingController,
        private nativeStorage: NativeStorage,
        public platform: Platform,
        public data: Data,
    ) {

        this.route.queryParams.subscribe((res) => {

            this.tempCountry = res.country;
            this.infaq = this.data.getInfaqType(res.infaqType);

            if (!this.countries) {
                this.getCountries();
            }
            this.resetCountry();
        });
    }

    @ViewChild('countrySelect', { static: false }) countrySelect: IonSelect;

    countryList = require('country-list');
    countrySelection;
    loading: any;
    res: any;
    loader = false;
    infaq = null;
    category = null;
    country = null;
    infaqType = null;
    cashType = null;
    cashReceiver = null;
    cashAmount = null;
    countries: any;
    countryChildrens: any;
    gridView = true;
    products: any = [];

    cashProduct: any;

    infaqCash: any;
    zakatCash: any;

    infaqTypes = [
        {
            id: 1,
            name: 'Goods',
            icon: 'basket'
        },
        {
            id: 2,
            name: 'Cash',
            icon: 'cash'
        }
    ];

    cashTypes = [
        {
            id: 1,
            name: 'Individual',
            icon: 'person'
        },
        {
            id: 2,
            name: 'Organisation',
            icon: 'people'
        }
    ];

    customActionSheetOptions: any = {
        header: 'Country',
        subHeader: 'Select your prefered country'
    };

    tempCountry: any;

    selectedCountry: any;

    ngOnInit() {

    }

    async presentLoading() {
        this.loading = await this.loadingController.create({
            message: 'Please wait...'
        });
        await this.loading.present();
    }

    dismissLoading() {
        if (this.loading) {
            this.loading.dismiss();
        }
    }

    resetCountry() {
        this.country = null;
        this.infaqType = null;
        this.cashType = null;
        this.cashReceiver = null;
    }

    async getInfaqType() {

        this.presentLoading();

        // 37 = infaq type parent
        await this.api.getItem('products/categories', {
            parent: 37,
        }).subscribe(res => {
            this.dismissLoading();
            if (res) {

                console.log('infaq type', res);
            }

        }, err => {
            console.log(err);
            this.dismissLoading();
        });

    }

    async getCountries() {

        const params = {
            parent: 22,
            order: 'asc',
            orderby: 'description',
        };

        if (this.platform.is('hybrid')) {
            await this.api.getItemIonic('products/categories', params).then((res) => {

                if (res) {
                    this.processCountry(res);
                }

            }, err => {
                console.log(err);
            });
        } else {
            await this.api.getItem('products/categories', params).subscribe(res => {

                if (res) {
                    this.processCountry(res);
                }

            }, err => {
                console.log(err);
            });
        }

    }

    processCountry(res) {

        const countries: any = res;

        countries.forEach(country => {
            if (!country.image) {
                country.image_path = '';
            } else {
                country.image_path = country.image.src;
            }


            console.log('this.data.categories', this.data.categories);
            console.log('obj.parent', country.id);

            const childrens = this.data.categories.filter(obj => {
                return obj.parent == country.id;
            });

            console.log('childrens', childrens);

            if (childrens) {
                country.childrens = childrens;

                country.childrens.forEach(children => {
                    children.image_path = children.image.src;
                });

                country.childrens.sort(this.sortCountry);
            }

        });

        this.countries = countries;
        console.log('countries', countries);

        this.countrySelection = this.countryList.getCodeList();

        countries.forEach(country => {
            if (this.tempCountry) {
                if (this.tempCountry === country.id) {
                    this.country = country;
                }
            }
        });

        this.nativeStorage.setItem('countries', countries).then(
            () => console.log('Stored item!'), error => console.error('Error storing item', error));
    }

    selectCountry(country, extra = null) {

        this.countryChildrens = null;

        if (country.childrens && country.childrens.length > 0) {

            this.countryChildrens = country.childrens;
            return;
        }

        if (country.slug === 'other-countries-user-select' && !extra) {
            this.selectedCountry = null;
            console.log('countries', this.countries);
            console.log(country);
            this.selectedCountry = country;
            this.countrySelect.open();
            return;
        }

        const params = {
            infaq_id: this.infaq.id,
            infaq_name: this.infaq.name,
            country_id: country.id,
            country_name: country.name,
            extra_country: extra
        };

        if (this.infaq.id === 18) {
            // select type
            // this.country = country;

            const navigationExtras: NavigationExtras = {
                queryParams: {
                    country,
                    infaq: this.infaq,
                    extra_country: extra,
                }
            };
            this.navCtrl.navigateForward('/tabs/home/infaq/infaq-type', navigationExtras);

        } else if (this.infaq.id === 19) {
            // redirect to products page

            // waqaf id = 19
            this.router.navigate(['/tabs/home/infaq/categories/products/' + this.infaq.id], {
                queryParams: params,
            });

        } else if (this.infaq.id === 20) {
            // redirect to tunai page

            this.router.navigate(['/tabs/home/infaq/cash/' + this.settings.settings.cash_zakat], {
                queryParams: params,
            });

        } else if (this.infaq.id === 21) {
            // redirect to products page

            // waqaf id = 19
            this.router.navigate(['/tabs/home/infaq/categories/products/' + this.infaq.id], {
                queryParams: params,
            });
        }

    }

    backToCountryList() {
        this.countryChildrens = null;
        console.log('back', this.countryChildrens);
    }

    allCountrySelect(event) {

        // this.data.global.extra_country = event.detail.value;
        console.log(event.detail.value);
        this.selectCountry(this.selectedCountry, event.detail.value);

    }

    deleteByValue(items, val) {
        for (const f in items) {
            if (items.hasOwnProperty(f) && items[f] === val) {
                delete items[f];
            }
        }
    }

    async getCashProduct(slugName) {

        console.log('get cash:' + slugName);

        const params = {
            slug: slugName,
            per_page: 1,
        };

        if (this.platform.is('hybrid')) {
            await this.api.getItemIonic('products', params).then((res) => {
                if (res) {
                    this.zakatCash = res[0];
                    this.nativeStorage.setItem('zakatCash', res[0]).then(
                        () => console.log('Stored item!'), error => console.error('Error storing item', error));
                }
                this.dismissLoading();
            }, err => {
                this.dismissLoading();
                console.log(err);
            });
        } else {
            await this.api.getItem('products', params).subscribe(res => {
                if (res[0]) {
                    this.zakatCash = res[0];
                    this.nativeStorage.setItem('zakatCash', res[0]).then(
                        () => console.log('Stored item!'), error => console.error('Error storing item', error));
                }
                this.dismissLoading();
            }, err => {
                this.dismissLoading();
                console.log(err);
            });
        }

    }

    selectInfaqType(infaqType) {

        if (infaqType.id === 1) {
            this.infaqType = infaqType;
            console.log(infaqType);
            this.redirectInfaq();
        }

        if (infaqType.id === 2) {

            const params = {
                infaq_id: this.infaq.id,
                infaq_name: this.infaq.name,
                country_id: this.country.id,
                country_name: this.country.name,
            };

            this.router.navigate(['/tabs/home/infaq/cash/' + this.settings.settings.cash_infaq], {
                queryParams: params,
            });
        }

    }

    getProducts() {

        const filter = {};

        this.loader = true;
        this.api.postFlutterItem('products', filter).subscribe(res => {
            console.log(res);
            this.products = res;
            this.loader = false;
        }, err => {
            console.log(err);
        });
    }

    redirectInfaq() {

        const params = {
            infaq_id: this.infaq.id,
            infaq_name: this.infaq.name,
            country_id: this.country.id,
            country_name: this.country.name,
            country_src: this.country.image.src,
        };

        console.log('redirect', this.router.url);
        this.router.navigate(['/tabs/home/infaq/categories'], {
            queryParams: params,
        });
    }

    sortCountry(a, b) {
        if (a.description < b.description) {
            return -1;
        }
        if (a.description > b.description) {
            return 1;
        }
        return 0;
    }

}
