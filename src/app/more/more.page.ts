import { Component } from '@angular/core';
import { LoadingController, NavController, ToastController } from '@ionic/angular';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from '../api.service';
import { Data } from '../data';
import { Settings } from '../data/settings';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';

@Component({
    selector: 'app-more',
    templateUrl: 'more.page.html',
    styleUrls: ['more.page.scss']
})
export class MorePage {

    loader = false;

    apps = [
        {
            name: 'Latest News',
            icon: '/assets/icon/subicon-news.png',
            link: '/tabs/more/news',
            category: '16'
        },
        {
            name: 'Image Gallery',
            icon: '/assets/icon/subicon-gallery.png',
            link: '/tabs/more/news',
            category: 52,
        },
        {
            name: 'Applicant Form',
            icon: '/assets/icon/subicon-applicant.png',
            externalLink: 'http://penerima.al-infaq.com/'
        },
        {
            name: 'Order History',
            icon: '/assets/icon/subicon-orderhistory.png',
            link: '/tabs/account/orders'
        },
        {
            name: 'Ramadan',
            icon: '/assets/icon/features/subicon-ramadan.png',
            link: '/tabs/more/news',
            category: 151,
        },
        {
            name: 'Alquran',
            icon: '/assets/icon/features/subicon-alquran.png',
            link: '/tabs/more/alquran',
        },
        {
            name: 'Qibla',
            icon: '/assets/icon/features/subicon-qibla.png',
            link: '/tabs/more/qibla'
        },
        {
            name: 'Prayer Time',
            icon: '/assets/icon/features/subicon-prayer-time.png',
            link: '/tabs/more/prayer'
        },
        {
            name: 'Tadhkirah',
            icon: '/assets/icon/features/subicon-tadhkirah.png',
            link: '/tabs/more/tadhkirah'
        },
        {
            name: 'Calendar',
            icon: '/assets/icon/features/subicon-calendar.png',
            link: '',
        },
        {
            name: 'Duas',
            icon: '/assets/icon/features/subicon-duas.png',
            link: '',
        },
        {
            name: 'Solah',
            icon: '/assets/icon/features/subicon-solah.png',
            link: ''
        }
    ];

    constructor(
        public api: ApiService,
        public data: Data,
        public loadingController: LoadingController,
        public navCtrl: NavController,
        public router: Router,
        public settings: Settings,
        public route: ActivatedRoute,
        public toastController: ToastController,
        private iab: InAppBrowser
    ) {

    }

    ionViewWillEnter() {

    }

    async presentToast(message) {
        const toast = await this.toastController.create({
            message,
            duration: 1000,
            position: 'top'
        });
        toast.present();
    }

    navigate(app) {

        let data = {};

        if (!app.link && !app.externalLink) {

            this.presentToast('Coming soon...');
            return;
        }

        if (app.externalLink) {

            const browser = this.iab.create(app.externalLink, '_system');

            return;
        }

        if (app.category) {
            data = {
                title: app.name,
                postCategory: app.category,
                origin: '/more'
            };
        } else {
            data = {
                title: app.name,
            };
        }

        this.router.navigate([app.link], {
            queryParams: data,
        });
    }
}
